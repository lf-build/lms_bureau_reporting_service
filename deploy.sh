#!/usr/bin/env bash

# set the docker image name
IMAGE_NAME=""
if [ -n "$1" ]
then
  IMAGE_NAME="$1"
else
  echo "You need to specify the name of image. Example: ./deploy.sh myservice"
  exit 1
fi

echo %teamcity.build.branch%

# set the image tag
IMAGE_TAG=""
if [ %teamcity.build.branch% = "uat-v1.0.0" ]
then
  IMAGE_TAG="uat"
else
  IMAGE_TAG="dev"
fi

# ensure is logged in into our registry
docker login -u lendfoundry -p lendfoundry -e "" registry.lendfoundry.com

echo $IMAGE_NAME:$IMAGE_TAG

# build the current project
docker build -t registry.lendfoundry.com/$IMAGE_NAME:$IMAGE_TAG . #:$IMAGE_TAG-$BUILD_NUMBER .

# publish the image to the registry
docker push registry.lendfoundry.com/$IMAGE_NAME:$IMAGE_TAG #:$IMAGE_TAG-$BUILD_NUMBER
