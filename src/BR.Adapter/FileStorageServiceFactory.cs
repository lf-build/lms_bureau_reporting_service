﻿using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.NumberGenerator.Client;
using LendFoundry.EventHub.Client;
using CreditExchange.StatusManagement.Client;
using LendFoundry.ProductConfiguration.Client;
using BR.Adapter.Abstractions;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.DataAttributes.Client;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using LMS.Loan.Filters.Abstractions;
using LendFoundry.Clients.DecisionEngine;

namespace BR.Adapter
{
    public class FileStorageServiceFactory : IFileStorageServiceFactory
    {
        public FileStorageServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public IFileStorageService Create(ITokenReader reader, ILogger logger)
        {
            var adapterRepositoryService = Provider.GetService<IAdapterRepositoryFactory>();
            var adapterRepository = adapterRepositoryService.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();


            var adapterConfigurationService = configurationServiceFactory.Create<AdapterConfiguration>(Abstractions.Settings.ServiceName, reader);
            var adapterConfiguration = adapterConfigurationService.Get();
            

            return new SftpService(adapterConfiguration, logger);
        }
    }
}
