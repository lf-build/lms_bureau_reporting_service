﻿using LendFoundry.Foundation.Date;
using System.Threading.Tasks;
using LendFoundry.NumberGenerator;
using CreditExchange.StatusManagement;
using System.Collections.Generic;
using System.Linq;
using System;
using LendFoundry.Foundation.Services;
using LendFoundry.EventHub.Client;
using LendFoundry.ProductConfiguration;
using BR.Adapter.Abstractions;
using LendFoundry.Foundation.Lookup;
using System.Dynamic;
using LendFoundry.DataAttributes;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using LMS.Loan.Filters.Abstractions;
using LendFoundry.Clients.DecisionEngine;
using Newtonsoft.Json;
using System.IO;

namespace BR.Adapter
{
    public class BureauService : IBureauService
    {
        #region Constructor

        public BureauService
        (
           AdapterConfiguration adapterConfiguration,
           ILoanScheduleService loanScheduleService,
           ILoanOnboardingService loanOnboardingService,
           IAccrualBalanceService accrualBalanceService,
           //IGeneratorService numberGeneratorService,
           IEntityStatusService statusManagementService,
           IProductService productService,
           IEventHubClient eventHubClient,
           ITenantTime tenantTime,
           //ILookupService lookup,
           IDataAttributesEngine dataAttributesEngine,
           IAdapterRepository adapterRepository,
           ILoanFilterService loanFilterService,
           BR.Adapter.Abstractions.IPaymentRepository paymentRepository,
           IScheduleRepository scheduleRepository,
           IAccrualDetailsRepository accrualDetailsRepository,
           BR.Adapter.Abstractions.IBureauRepository bureauRepository,
           IDecisionEngineService decisionEngineService,
           IBureauFileRepository bureauFileRepository,
           IFileStorageService fileStorageService
        )
        {
            if (adapterConfiguration == null)
                throw new ArgumentException("Configuration not set");

            AdapterConfiguration = adapterConfiguration;
            LoanScheduleService = loanScheduleService;
            //NumberGeneratorService = numberGeneratorService;
            ProductService = productService;
            StatusManagementService = statusManagementService;
            //EventHubClient = eventHubClient;
            TenantTime = tenantTime;
            //Lookup = lookup;
            DataAttributesEngine = dataAttributesEngine;
            AccrualBalanceService = accrualBalanceService;
            AdapterRepository = adapterRepository;
            LoanOnboardingService = loanOnboardingService;
            LoanFilterService = loanFilterService;
            PaymentRepository = paymentRepository;
            ScheduleRepository = scheduleRepository;
            AccrualDetailsRepository = accrualDetailsRepository;
            BureauRepository = bureauRepository;
            DecisionEngineService = decisionEngineService;
            BureauFileRepository = bureauFileRepository;
            FileStorageService = fileStorageService;

        }

        #endregion

        #region Variables
        private IAccrualBalanceService AccrualBalanceService { get; set; }
        private AdapterConfiguration AdapterConfiguration { get; }
        private ILoanScheduleService LoanScheduleService { get; }
        private ILoanOnboardingService LoanOnboardingService { get; }
        private ILoanFilterService LoanFilterService { get; }
        private ITenantTime TenantTime { get; }
        private IAdapterRepository AdapterRepository { get; }
        private IScheduleRepository ScheduleRepository { get; }
        private IAccrualDetailsRepository AccrualDetailsRepository { get; }
        private IGeneratorService NumberGeneratorService { get; }
        private IEventHubClient EventHubClient { get; }
        private IEntityStatusService StatusManagementService { get; }
        private IProductService ProductService { get; }
        private ILookupService Lookup { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private BR.Adapter.Abstractions.IPaymentRepository PaymentRepository { get; }
        private IBureauRepository BureauRepository { get; }
        private IBureauFileRepository BureauFileRepository { get; }
        private IDecisionEngineService DecisionEngineService { get; }

        private IFileStorageService FileStorageService { get; set; }

        #endregion

        public async Task<string> ProcessLoans()
        {

            string fileId = Guid.NewGuid().ToString("N");

            var loans = await AdapterRepository.GetLoansList();
            TimeBucket todaysDate = new TimeBucket(TenantTime.Now);
            if (AdapterConfiguration.IsFilterDate.Value == true && AdapterConfiguration.ProcessingDate != null)
            {
                todaysDate = new TimeBucket(AdapterConfiguration.ProcessingDate.Value);
            }

            if (loans.Count() > 0)
            {
                string filename = loans.FirstOrDefault().TenantId + "_BureauReport_" + TenantTime.Now.Month.ToString() + "_" + TenantTime.Now.Day.ToString() + "_" + TenantTime.Now.Year.ToString();

                IBureauFile fileinfo = new BureauFile
                {
                    FileId = fileId,
                    FileName = filename,
                    CreatedDate = todaysDate
                };

                BureauFileRepository.Add(fileinfo);
            }

            foreach (var item in loans.OrderByDescending(x => x.LoanNumber))
            {

                var loanInformation = await LoanOnboardingService.GetLoanInformationByLoanNumber(item.LoanNumber);
                var schedules = await ScheduleRepository.GetScheduleByLoanNumber(item.LoanNumber);
                var accrual = await AccrualDetailsRepository.GetAccrualByLoanNumber(item.LoanNumber);
                var payments = await PaymentRepository.GetPayments(item.LoanNumber);

                var previousrecord = await BureauRepository.GetLoanInformationByLoanNumber(item.LoanNumber);
                var statusinfo = await StatusManagementService.GetStatusTransitionHistory("loan", item.LoanNumber, item.LoanProductId);


                string oldpaymentprofile = "";
                string paymentprofile = "D";
                DateTime lastreportingdate = TenantTime.Now.DateTime;
                var consumerTransactionType = "";

                IBaseSegment lastreportdetails = null;

                if (previousrecord != null && previousrecord.BaseSegments != null)
                {
                    var previousbasesegment = previousrecord.BaseSegments;
                    lastreportdetails = previousbasesegment.OrderByDescending(x => x.LastReportDate.Time.DateTime).FirstOrDefault();

                    if (lastreportdetails.LastReportDate != null)
                    {
                        oldpaymentprofile = lastreportdetails.PaymentHistoryProfile;
                        if (lastreportdetails.LastReportDate.Time.Date != DateTime.MinValue)
                        {
                            lastreportingdate = lastreportdetails.LastReportDate.Time.Date;
                        }
                    }
                }
                else
                {
                    consumerTransactionType = "1";
                    lastreportingdate = lastreportingdate.AddDays(-90);
                }

                var statuslist = statusinfo.Where(x => x.ActivedOn.Time.Date > lastreportingdate.Date && x.ActivedOn.Time.Date <= todaysDate.Time.Date).Select(y => y.Status).ToList();

                IBureauMaster reportInfo = new BureauMaster
                {
                    LoanNumber = item.LoanNumber
                };
                reportInfo.BaseSegments = new List<IBaseSegment>();

                var products = await GetProductParameters(item.LoanProductId, item.LoanNumber);
                var bankrupty = await GetBankruptcy(item.LoanNumber);

                string newguid = Guid.NewGuid().ToString("N");

                var basesegment = new BaseSegment
                {
                    Id = newguid,
                    ConsumerAccountNumber = item.LoanNumber,
                    PortfolioType = products.Product.PortfolioType.ToString().Substring(0, 1),
                    // AccountType = AdapterConfiguration.AccountType, ///remaining
                    TermsFrequency = string.IsNullOrEmpty(item.PaymentFrequency) ? "" : item.PaymentFrequency.Substring(0, 1),
                    ECOACode = AdapterConfiguration.ECOACode,   ///remaining 
                    DateOpened = item.LoanFundedDate,
                    CreditLimit = 0,
                    HighestCredit = item.LoanAmount,
                    TermsDuration = item.Tenure,

                    //PaymentHistoryProfile ///remaining
                    //ComplianceConditionCode ///remaining

                    DateofAccountInformation = new TimeBucket(TenantTime.Now),
                    CycleIdentifier = "0",
                    //ConsumerTransactionType=///remaining //need to compute
                    SurName = item.PrimaryApplicantLastName,
                    FirstName = item.PrimaryApplicantFirstName,
                    MiddleName = item.PrimaryApplicantMiddle,
                    GenerationCode = string.IsNullOrEmpty(item.PrimaryApplicantGeneration) ? "" : item.PrimaryApplicantGeneration.Substring(0, 1),
                    SSN = item.PrimaryApplicantSSN,
                    DateofBirth = item.PrimaryApplicantDOB,
                    TelephoneNumber = item.PrimaryApplicantPhoneNo,

                    CountryCode = AdapterConfiguration.CountryCode,
                    FirstLineOfAddress = item.PrimaryApplicantAddressLine1,
                    SecondLineOfAddress = item.PrimaryApplicantAddressLine2,
                    City = item.PrimaryApplicantCity,
                    State = item.PrimaryApplicantState,
                    ZipCode = item.PrimaryApplicantZip,
                    LastReportDate = todaysDate,
                    FileId = fileId
                };

                if (products != null && products.ProductParameters.Count() > 0)
                {
                    var productdetails = products.ProductParameters.Where(x => x.Name == "INTEREST_RATE_TYPE");
                    if (productdetails != null)
                    {
                        basesegment.InterestTypeIndicator = productdetails.Select(y => y.Value).FirstOrDefault().Substring(0, 1);
                    }
                }
                foreach (var iteminfo in loanInformation.ApplicantDetails)
                {
                    if (iteminfo.Addresses != null)
                    {
                        if (iteminfo.Addresses.Any(x => x.IsActive))
                        {
                            basesegment.AddressIndicator = "C";
                        }
                    }
                }

                var feesOutStanding = 0.0;
                if (item.PBOT != null)
                {
                    basesegment.CurrentBalance = item.PBOT.TotalPrincipalOutStanding + item.PBOT.TotalInterestOutstanding + item.PBOT.AdditionalOutStandingInterest;
                    feesOutStanding = item.PBOT.TotalFeeOutstanding;
                }
                if (item.PBOC != null)
                {
                    basesegment.AmountPastDue = item.PBOC.CurrentPrincipalOutStanding + item.PBOC.CurrentInterestOutStanding + feesOutStanding;
                }
                if (item.PaymentInfo != null && item.PaymentInfo.LastPaymentReceivedDate != DateTime.MinValue)
                {
                    basesegment.DateofLastPayment = new TimeBucket(item.PaymentInfo.LastPaymentReceivedDate);
                }
                if (item.ProductPortfolioType.ToUpper() == ProductPortfolioType.LineOfCredit.ToString().ToUpper())
                {
                    basesegment.CreditLimit = item.CreditLimit;
                    basesegment.HighestCredit = basesegment.CreditLimit;
                }

                if (products.Product.ProductCategory == LendFoundry.ProductConfiguration.ProductCategory.Individual && products.Product.PortfolioType == PortfolioType.Installment)
                {
                    basesegment.AccountType = "01";
                }
                else if (products.Product.ProductCategory == LendFoundry.ProductConfiguration.ProductCategory.Individual && products.Product.PortfolioType == PortfolioType.LineOfCredit)
                {
                    basesegment.AccountType = "15";
                }
                else if (products.Product.ProductCategory == LendFoundry.ProductConfiguration.ProductCategory.Business && products.Product.PortfolioType == PortfolioType.LineOfCredit)
                {
                    basesegment.AccountType = "9B";
                }
                else if (products.Product.ProductCategory == LendFoundry.ProductConfiguration.ProductCategory.Business && products.Product.PortfolioType == PortfolioType.Installment)
                {
                    basesegment.AccountType = "10";
                }

                if (item.PayOff != null)
                {
                    if (item.PBOT.TotalPrincipalOutStanding == 0 && item.PBOT.TotalInterestOutstanding == 0)
                    {
                        basesegment.ComplianceConditionCode = "XA";
                    }
                }

                var scheduledMonthlyPaymentAmount = 0.0;

                // schedules.Scheduledetails.Where(i => i.loanDetails.ScheduleDate >= lastreportingdate && i.loanDetails.ScheduleDate < lastreportingdate).Sum(x => x.loanDetails.PaymentAmount);
                foreach (var scheduleitem in schedules.Scheduledetails)
                {
                    if (scheduleitem.loanDetails.ScheduleDate > lastreportingdate && scheduleitem.loanDetails.ScheduleDate <= todaysDate.Time.Date)
                    {
                        scheduledMonthlyPaymentAmount += scheduleitem.loanDetails.PaymentAmount;
                    }
                }

                //var actualPaymentAmount = schedules.Scheduledetails.Where(i => i.schduleTreckingDetails.Any(u => u.PaymentDate >= lastreportingdate && u.PaymentDate < lastreportingdate && u.IsPaid == true)).Select(x => x.schduleTreckingDetails.Sum(a => a.PaymentAmount)).Sum();
                var actualPaymentAmount = 0.0;
                if (payments != null)
                {
                    actualPaymentAmount = payments.Where(i => i.PaymentDate > lastreportingdate && i.PaymentDate <= todaysDate.Time.Date).Select(y => y.AmountPaid).Sum();
                }
                //var scheduletracking = schedules.Scheduledetails.Select(x => x.schduleTreckingDetails);

                //var paymenthistoryprofile = schedules.Scheduledetails.Where(i => i.schduleTreckingDetails.Any(u => u.PaymentDate >= lastreportingdate && u.PaymentDate < lastreportingdate && u.IsPaid == true)).Select(x => x.schduleTreckingDetails.Sum(a => a.PaymentAmount)).Sum();

                basesegment.ScheduledMonthlyPaymentAmount = scheduledMonthlyPaymentAmount;
                basesegment.ActualPaymentAmount = actualPaymentAmount;

                DateTime lastPayementDate = new DateTime();
                basesegment.AccountStatus = "11";
                if (accrual != null)
                {
                    if (accrual.PaymentInfo != null && accrual.PaymentInfo.LastPaymentReceivedDate != DateTime.MinValue)
                    {
                        lastPayementDate = accrual.PaymentInfo.LastPaymentReceivedDate;
                        basesegment.DateofLastPayment = new TimeBucket(lastPayementDate);
                    }

                    if (accrual.Due != null)
                    {
                        if (accrual.Due.DPDStartDate != DateTime.MinValue)
                        {
                            basesegment.DateofFirstDelinquency = new TimeBucket(accrual.Due.DPDStartDate);
                        }
                        if (accrual.Due.DPDDays == 0)
                        {
                            basesegment.PaymentRating = "0";
                            basesegment.AccountStatus = "11";
                            paymentprofile = "0";
                        }
                        else if (accrual.Due.DPDDays > 0 && accrual.Due.DPDDays <= 29)
                        {
                            basesegment.PaymentRating = "0";
                            basesegment.AccountStatus = "11";
                        }
                        else if (accrual.Due.DPDDays >= 30 && accrual.Due.DPDDays <= 59)
                        {
                            basesegment.PaymentRating = "1";
                            basesegment.AccountStatus = "71";
                            paymentprofile = "1";
                        }
                        else if (accrual.Due.DPDDays >= 60 && accrual.Due.DPDDays <= 89)
                        {
                            basesegment.PaymentRating = "2";
                            basesegment.AccountStatus = "78";
                            paymentprofile = "2";

                        }
                        else if (accrual.Due.DPDDays >= 90 && accrual.Due.DPDDays <= 119)
                        {
                            basesegment.PaymentRating = "3";
                            basesegment.AccountStatus = "80";
                            paymentprofile = "3";
                        }
                        else if (accrual.Due.DPDDays >= 120 && accrual.Due.DPDDays <= 149)
                        {
                            basesegment.PaymentRating = "4";
                            basesegment.AccountStatus = "82";
                            paymentprofile = "4";
                        }
                        else if (accrual.Due.DPDDays >= 150 && accrual.Due.DPDDays <= 179)
                        {
                            basesegment.PaymentRating = "5";
                            basesegment.AccountStatus = "83";
                            paymentprofile = "5";
                        }
                        else if (accrual.Due.DPDDays >= 180)
                        {
                            basesegment.PaymentRating = "6";
                            basesegment.AccountStatus = "84";
                            paymentprofile = "6";
                        }
                    }
                    var chargeoffamount = 0.0;
                    if (item.ChargeOff != null)
                    {
                        chargeoffamount = item.ChargeOff.ChargeOffAmount;
                    }

                    if ((item.StatusCode == "200.30" || (item.StatusCode == "200.60" && statuslist.Contains("200.30"))) && schedules.ScheduledCreatedReason.ToUpper() == AdapterConfiguration.VersionReason && schedules.ModReason.ToUpper() == AdapterConfiguration.SubReason.ToUpper() && chargeoffamount <= 0.0 /*&& and loan has version with version reason = 'modification' and subreason = 'collection' and chargedoff amount <= 0*/)
                    {
                        basesegment.AccountStatus = "62";
                    }
                    else if ((item.StatusCode == "200.30" || (item.StatusCode == "200.60" && statuslist.Contains("200.30"))) && chargeoffamount > 0.0)
                    {
                        basesegment.AccountStatus = "64";
                        //if (item.ChargeOff != null && item.PBOT != null)
                        //{
                        //    if (item.PBOT.TotalPrincipalOutStanding == 0 && item.PBOT.TotalInterestOutstanding == 0)
                        //    {
                        //        basesegment.AccountStatus = "64";
                        //    }
                        //}
                    }
                    else if (item.StatusCode == "200.30" || (item.StatusCode == "200.60" && statuslist.Contains("200.30")))
                    {
                        basesegment.AccountStatus = "13";
                    }
                    if (!string.IsNullOrEmpty(schedules.ScheduledCreatedReason) && !string.IsNullOrEmpty(schedules.ModReason) && !string.IsNullOrEmpty(AdapterConfiguration.VersionReason) && !string.IsNullOrEmpty(AdapterConfiguration.SubReason))
                    {
                        if (schedules.ScheduledCreatedReason.ToUpper() == AdapterConfiguration.VersionReason.ToUpper() && schedules.ModReason.ToUpper() == AdapterConfiguration.SubReason.ToUpper())
                        {
                            //If payment in collection then PAymentRating="G";
                            basesegment.PaymentRating = "G";
                            paymentprofile = "G";
                        }
                    }
                    if (accrual.PayOff != null)
                    {
                        if (accrual.PayOff.PayOffDate != null && accrual.PayOff.PayOffDate != DateTime.MinValue)
                        {
                            basesegment.DateClosed = new TimeBucket(accrual.PayOff.PayOffDate);
                        }
                    }

                    if (accrual.ChargeOff != null && (item.StatusCode == "200.60" || item.StatusCode.ToUpper() == "200.40") && accrual.ChargeOff.ChargeOffAmount > 0)
                    {
                        basesegment.PaymentRating = "L";
                        paymentprofile = "L";
                        basesegment.OriginalChargeoffAmount = accrual.ChargeOff.ChargeOffAmount;
                        basesegment.AccountStatus = "97";
                    }

                    /*
                   for 62
                    [loan status is paid off or (status=closed and prior status is paid off )] and loan has version  with version reason ='modification' and subreason='collection' 
                    and chargedoff amount <=0

                    for 64
                    [loan status is paid off or (status=closed and prior status is paid off )] and chargedoff amount>0

                    for 13
                    [loan status is paid off or (status=closed and prior status is paid off )] and not 62 and not 64

                    for 97
                    [loan status is closed and chargedoff amount>0]  
                 */

                }

                basesegment.CorrectionIndicator = "0"; //TODO remaining


                /* Rule Logic
                var executionResult = DecisionEngineService.Execute<dynamic, MapAccountDetails>("MapAccountDetails", new { accrual = accrual, });

                reportdetail.AccountStatus = executionResult.AccountStatus;
                paymentprofile = executionResult.PaymentProfile;
                reportdetail.PaymentRating = executionResult.PaymentRating;
                */

                //TODO : add condition for collection 
                //if (accrual.ChargeOff != null)
                //{
                //    reportdetail.PaymentRating = "L";
                //}

                if (!string.IsNullOrEmpty(oldpaymentprofile))
                {
                    oldpaymentprofile = oldpaymentprofile.Substring(0, oldpaymentprofile.Length - 1);
                }
                else
                {
                    oldpaymentprofile = "BBBBBBBBBBBBBBBBBBBBBBB";
                }
                basesegment.PaymentHistoryProfile = paymentprofile + oldpaymentprofile;

                #region Compliance Condition Code - Bankruptcy
                string bankruptystatus = "";
                string bankruptychapter = "";
                if (bankrupty != null)
                {
                    bankruptystatus = bankrupty.caseStatus;
                    bankruptychapter = bankrupty.chapter;
                }

                switch (bankruptystatus.ToUpper())
                {
                    case "PETITION":
                        switch (bankruptychapter.ToUpper())
                        {
                            case "7":
                                basesegment.ConsumerInformationIndicator = "A";
                                break;
                            case "11":
                                basesegment.ConsumerInformationIndicator = "B";
                                break;
                            case "12":
                                basesegment.ConsumerInformationIndicator = "C";
                                break;
                            case "13":
                                basesegment.ConsumerInformationIndicator = "D";
                                break;
                            default:
                                break;
                        }
                        break;
                    case "DISCHARGED":
                        switch (bankruptychapter)
                        {
                            case "7":
                                basesegment.ConsumerInformationIndicator = "E";
                                break;
                            case "11":
                                basesegment.ConsumerInformationIndicator = "F";
                                break;
                            case "12":
                                basesegment.ConsumerInformationIndicator = "G";
                                break;
                            case "13":
                                basesegment.ConsumerInformationIndicator = "H";
                                break;
                            default:
                                break;
                        }
                        break;
                    case "DISMISSED":
                        switch (bankruptychapter)
                        {
                            case "7":
                                basesegment.ConsumerInformationIndicator = "I";
                                break;
                            case "11":
                                basesegment.ConsumerInformationIndicator = "J";
                                break;
                            case "12":
                                basesegment.ConsumerInformationIndicator = "K";
                                break;
                            case "13":
                                basesegment.ConsumerInformationIndicator = "L";
                                break;
                            default:
                                break;
                        }
                        break;
                    case "WITHDRAWN":
                        switch (bankruptychapter)
                        {
                            case "7":
                                basesegment.ConsumerInformationIndicator = "M";
                                break;
                            case "11":
                                basesegment.ConsumerInformationIndicator = "N";
                                break;
                            case "12":
                                basesegment.ConsumerInformationIndicator = "O";
                                break;
                            case "13":
                                basesegment.ConsumerInformationIndicator = "P";
                                break;
                            default:
                                break;
                        }
                        break;
                    case "PERSONAL RECEIVERSHIP":
                        basesegment.ConsumerInformationIndicator = "1A";
                        break;
                    default:
                        //TODO for  Q,T and U  //Remaining
                        break;
                }

                #endregion

                #region Consumer Transcaction Type 
                bool namechange = false;
                bool addresschange = false;
                bool ssnchange = false;

                if (lastreportdetails != null)
                {
                    if (!comparestring(item.PrimaryApplicantFirstName, lastreportdetails.FirstName) || !comparestring(item.PrimaryApplicantMiddle, lastreportdetails.MiddleName) || !comparestring(item.PrimaryApplicantLastName, lastreportdetails.SurName))
                    {
                        namechange = true;
                        consumerTransactionType = "2";
                    }
                    if (!comparestring(item.PrimaryApplicantAddressLine1, lastreportdetails.FirstLineOfAddress) || !comparestring(item.PrimaryApplicantAddressLine2, lastreportdetails.SecondLineOfAddress) || !comparestring(item.PrimaryApplicantCity, lastreportdetails.City) || !comparestring(item.PrimaryApplicantState, lastreportdetails.State) || !comparestring(item.PrimaryApplicantZip, lastreportdetails.ZipCode))
                    {
                        addresschange = true;
                        consumerTransactionType = "3";
                    }
                    if (!comparestring(item.PrimaryApplicantSSN, lastreportdetails.SSN))
                    {
                        ssnchange = true;
                        consumerTransactionType = "5";
                    }
                    if (namechange && addresschange)
                    {
                        consumerTransactionType = "6";
                    }
                    if (namechange && ssnchange)
                    {
                        consumerTransactionType = "8";
                    }
                    if (addresschange && ssnchange)
                    {
                        consumerTransactionType = "9";
                    }
                    if (namechange && addresschange && ssnchange)
                    {
                        consumerTransactionType = "A";
                    }
                }

                basesegment.ConsumerTransactionType = consumerTransactionType;
                #endregion

                basesegment.DateofAccountInformation = new TimeBucket(TenantTime.Now);

                reportInfo.BaseSegments.Add(basesegment);

                reportInfo.J1s = new List<IJOne>();
                reportInfo.J2s = new List<IJTwo>();
                reportInfo.K1 = new List<IKOne>();
                reportInfo.K2 = new List<IKTwo>();
                reportInfo.K3 = new List<IKThree>();
                reportInfo.K4 = new List<IKFour>();
                reportInfo.N1 = new List<INOne>();
                reportInfo.L1 = new List<ILOne>();
                var j1 = new JOne
                {
                    Id = newguid,
                    J11 = "",
                    J12 = "",
                    J13 = "",
                    LastReportDate = todaysDate,
                    FileId = fileId
                };
                reportInfo.J1s.Add(j1);

                var j2 = new JTwo
                {
                    Id = newguid,
                    J21 = "",
                    J22 = "",
                    J23 = "",
                    LastReportDate = todaysDate,
                    FileId = fileId
                };
                reportInfo.J2s.Add(j2);

                var k1 = new KOne
                {
                    Id = newguid,
                    K1 = "",
                    LastReportDate = todaysDate,
                    FileId = fileId
                };
                reportInfo.K1.Add(k1);

                var k2 = new KTwo
                {
                    Id = newguid,
                    K2 = "",
                    LastReportDate = todaysDate,
                    FileId = fileId
                };
                reportInfo.K2.Add(k2);

                var k3 = new KThree
                {
                    Id = newguid,
                    K3 = "",
                    LastReportDate = todaysDate,
                    FileId = fileId
                };
                reportInfo.K3.Add(k3);

                var k4 = new KFour
                {
                    Id = newguid,
                    K4 = "",
                    LastReportDate = todaysDate,
                    FileId = fileId
                };
                reportInfo.K4.Add(k4);

                var l1 = new LOne
                {
                    Id = newguid,
                    L1 = "",
                    LastReportDate = todaysDate,
                    FileId = fileId
                };
                reportInfo.L1.Add(l1);

                var n1 = new NOne
                {
                    Id = newguid,
                    N1 = "",
                    LastReportDate = todaysDate,
                    FileId = fileId
                };
                reportInfo.N1.Add(n1);

                if (previousrecord == null)
                {
                    reportInfo.BureauCreateDate = todaysDate;
                    BureauRepository.Add(reportInfo);
                }
                else
                    await BureauRepository.AddReport(item.LoanNumber, basesegment, j1, j2, k1, k2, k3, k4, l1, n1);

            }



            return "";
        }

        public async Task<string> CreateFile(string fileId)
        {
            var fileinfo = await BureauFileRepository.GetFileByFileId(fileId);
            if (fileinfo == null)
                throw new ArgumentException($"file information does not exist for FileId: {fileId}");

            string filename = fileinfo.FileName;

            //  System.IO.FileStream fs = new System.IO.FileStream("D:\\ReportFiles\\" + filename + ".txt", System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
            System.IO.MemoryStream fs = new System.IO.MemoryStream();
            System.IO.StreamWriter writer = new System.IO.StreamWriter(fs);

            var loans = await BureauRepository.GetLoanListforFile(fileId);

            if (loans == null)
                throw new ArgumentException($"loan information does not exist for FileId: {fileId}");

            int totalBaseRecords = 0;
            int totalofStatusCodeDF = 0;
            int totalJ1 = 0;
            int totalJ2 = 0;
            int blockCount = 0;
            int totalofStatusCodeDA = 0;
            int totalofStatusCode05 = 0;
            int totalofStatusCode11 = 0;
            int totalofStatusCode13 = 0;
            int totalofStatusCode61 = 0;
            int totalofStatusCode62 = 0;
            int totalofStatusCode63 = 0;
            int totalofStatusCode64 = 0;
            int totalofStatusCode65 = 0;
            int totalofStatusCode67 = 0;
            int totalofStatusCode71 = 0;
            int totalofStatusCode78 = 0;
            int totalofStatusCode80 = 0;
            int totalofStatusCode82 = 0;
            int totalofStatusCode83 = 0;
            int totalofStatusCode84 = 0;
            int totalofStatusCode88 = 0;
            int totalofStatusCode89 = 0;
            int totalofStatusCode93 = 0;
            int totalofStatusCode94 = 0;
            int totalofStatusCode95 = 0;
            int totalofStatusCode96 = 0;
            int totalofStatusCode97 = 0;
            int totalofECOACode = 0;
            int totalEmploymentSegments = 0;
            int totalOriginalCreditorSegments = 0;
            int totalPurchasedPortfolioORSoldToSegments = 0;
            int totalMortgageInformationSegments = 0;
            int totalSpecializedPaymentInformationSegments = 0;
            int totalChangeSegments = 0;
            int totalSSNsforAll = 0;
            int totalSSNsforBaseSegment = 0;
            int totalSSNsforJ1 = 0;
            int totalSSNsforJ2 = 0;
            int totalDOBsforAll = 0;
            int totalDOBsforBaseSegment = 0;
            int totalDOBsforJ1 = 0;
            int totalDOBsforJ2 = 0;
            int totalphonesforAll = 0;
            int totalphonesforBaseSegment = 0;
            int totalphonesforJ1 = 0;
            int totalphonesforJ2 = 0;

            string headerrecord = "";
            string currentrecord = "";
            string trailorrecord = "";

            #region Header record

            {                                   //-----------------------------------------------         POSITIOM    From -  To
                headerrecord = "#LN#";          //-----------------------------------------------                       1  -   4
                headerrecord += "HEADER";       //-----------------------------------------------                       5  -  10
                headerrecord += PadRight("", ' ', 2); //Cycle Number//---------------------------                       11 -  12
                headerrecord += PadRight(AdapterConfiguration.InnovisProgramIdentifier, ' ', 10);//                     13 -  22
                headerrecord += PadRight(AdapterConfiguration.EquifaxProgramIdentifier, ' ', 10);//                     23 -  32
                headerrecord += PadRight(AdapterConfiguration.ExperianProgramIdentifier, ' ', 5);//                     33 -  37
                headerrecord += PadRight(AdapterConfiguration.TransUnionProgramIdentifier, ' ', 10);// ---------------  38 -  47

                headerrecord += PadLeft(TenantTime.Now.ToString("MMddyyyy"), ' ', 8); //Activity Date ------------------  38 -  55
                headerrecord += PadLeft(TenantTime.Now.ToString("MMddyyyy"), ' ', 8); //Date Created  ------------------  56 -  63
                headerrecord += PadLeft(TenantTime.Now.ToString("MMddyyyy"), ' ', 8); //Program Date  ------------------  64 -  71
                headerrecord += PadLeft(TenantTime.Now.ToString("MMddyyyy"), ' ', 8); //Program RevisionDate  ----------  72 -  79

                headerrecord += PadRight(AdapterConfiguration.ReporterName, ' ', 40);// ------------------              80 - 119
                headerrecord += PadRight(AdapterConfiguration.ReporterAddress, ' ', 96);// ---------------             120 - 215
                headerrecord += PadLeft(AdapterConfiguration.ReporterTelephoneNumber, ' ', 10);// --------             216 - 225
                headerrecord += PadRight(AdapterConfiguration.SoftwareVendorName, ' ', 40);// ------------             226 - 265
                headerrecord += PadRight(AdapterConfiguration.SoftwareVersionNumber, ' ', 5);// ------------           266 - 270
                headerrecord += PadRight("", ' ', 156);// Reserved  ----------------------------------------           271 - 426
                headerrecord += PadRight("", ' ', 1238);// -------------------------------------------------           427 - 1664

                headerrecord = headerrecord.Replace("#LN#", PadLeft(headerrecord.Length.ToString(), '0', 4));

            }

            #endregion

            writer.Write(headerrecord + Environment.NewLine);

            foreach (var item in loans)
            {
                #region Base Segment
                var basesegments = item.BaseSegments;

                var reportdetails = basesegments.OrderByDescending(x => x.LastReportDate.Time.DateTime).FirstOrDefault();
                //----------------------------------------------- ------------------------------------------        POSITIOM     From -  To
                currentrecord = "#LN#";         //-----------------------------------------------                                   1 -   4
                currentrecord += AdapterConfiguration.ProcessingIndicator;   //ProcessingIndicator //-----------------------------  5 -   5

                currentrecord += PadLeft(TenantTime.Now.ToString("MMddyyyyhhmmss"), ' ', 14); //Time Stamp------------------------    6 -  19
                currentrecord += PadLeft(reportdetails.CorrectionIndicator, '0', 1);            //--------                         20 -  20
                currentrecord += PadRight(AdapterConfiguration.IdentificationNumber, ' ', 20);  //------------------               21 -  40
                currentrecord += PadRight(reportdetails.CycleIdentifier, ' ', 2); //Cycle Identifier-----------------              41 -  42
                currentrecord += PadRight(reportdetails.ConsumerAccountNumber, ' ', 30); //ConsumerAccountNumber----               43 -  72
                currentrecord += PadRight(reportdetails.PortfolioType, ' ', 1); //PortfolioType --------------------               73 -  73
                currentrecord += PadRight(AdapterConfiguration.AccountType, ' ', 2); //AccountType -----------------               74 -  75
                currentrecord += PadLeft(reportdetails.DateOpened.Time.ToString("MMddyyyy"), '0', 8); //DateOpened  ---            76 -  83
                currentrecord += PadLeft(reportdetails.CreditLimit.ToString(), '0', 9); //CreditLimit   ---------------            84 -  92
                currentrecord += PadLeft(reportdetails.HighestCredit.ToString(), '0', 9); //HighestCredit  ------------            93 - 101
                currentrecord += PadRight(reportdetails.TermsDuration.ToString(), ' ', 3); //TermsDuration  -----------           102 - 104
                currentrecord += PadRight(reportdetails.TermsFrequency, ' ', 1); //TermsFrequency ---------------------           105 - 105
                currentrecord += PadLeft(reportdetails.ScheduledMonthlyPaymentAmount.ToString(), '0', 9);//ScheduledMonthlyPaymentAmount--  106 - 114
                currentrecord += PadLeft(reportdetails.ActualPaymentAmount.ToString(), '0', 9);//ActualPaymentAmount --           115 - 123
                currentrecord += PadRight(reportdetails.AccountStatus, ' ', 2);//AccountStatus -------------------------          124 - 125
                currentrecord += PadRight(reportdetails.PaymentRating, ' ', 1);//PaymentRating -------------------------          126 - 126
                currentrecord += PadRight(reportdetails.PaymentHistoryProfile, ' ', 24);//PaymentHistoryProfile------------       127 - 150
                currentrecord += PadRight(reportdetails.SpecialComment, ' ', 2);//SpecialComment------------                      151 - 152
                currentrecord += PadRight(reportdetails.ComplianceConditionCode, ' ', 2);//ComplianceConditionCode------------    153 - 154
                currentrecord += PadLeft(reportdetails.CurrentBalance.ToString(), '0', 9);//CurrentBalance-                       155 - 163
                currentrecord += PadLeft(reportdetails.AmountPastDue.ToString(), '0', 9);//AmountPastDue  -                       164 - 172
                currentrecord += PadLeft(reportdetails.OriginalChargeoffAmount.ToString(), '0', 9);//OriginalChargeoffAmount-     173 - 181
                currentrecord += PadLeft(reportdetails.DateofAccountInformation.Time.ToString("MMddyyyy"), '0', 8);//DateofAccountInformation-- 182 - 189

                if (reportdetails.DateofFirstDelinquency == null || reportdetails.DateofFirstDelinquency.Time.Date == DateTime.MinValue)
                {
                    currentrecord += PadLeft("", '0', 8);   //DateofFirstDelinquency                                              190 - 197
                }
                else
                {
                    currentrecord += PadLeft(reportdetails.DateofFirstDelinquency.Time.ToString("MMddyyyy"), '0', 8);//DateofFirstDelinquency   190 - 197
                }

                if (reportdetails.DateClosed == null || reportdetails.DateClosed.Time.Date == DateTime.MinValue)
                {
                    currentrecord += PadLeft("", '0', 8);              //                        DateClosed -----------------     198 - 205
                }
                else
                {
                    currentrecord += PadLeft(reportdetails.DateClosed.Time.ToString("MMddyyyy"), '0', 8);//DateClosed -------     198 - 205
                }

                if (reportdetails.DateofLastPayment == null || reportdetails.DateofLastPayment.Time.Date == DateTime.MinValue)
                {
                    currentrecord += PadLeft("0", '0', 8);              //                  DateofLastPayment ----------------    206 - 213
                }
                else
                {
                    currentrecord += PadLeft(reportdetails.DateofLastPayment.Time.Date.ToString("MMddyyyy"), '0', 8);//DateofLastPayment-- 206 - 213
                }

                currentrecord += PadRight(reportdetails.InterestTypeIndicator, ' ', 1);//InterestTypeIndicator-----------------   214 - 214
                // ?????????

                currentrecord += PadRight("", ' ', 16);              //                  Reserved        -----------------        215 - 230

                currentrecord += PadRight(reportdetails.ConsumerTransactionType, ' ', 1);//ConsumerTransactionType--------------- 231 - 231
                currentrecord += PadRight(reportdetails.SurName, ' ', 25);//SurName---------------------------------------------- 232 - 256
                currentrecord += PadRight(reportdetails.FirstName, ' ', 20);//FirstName------------------------------------------ 257 - 276
                currentrecord += PadRight(reportdetails.MiddleName, ' ', 20);//MiddleName---------------------------------------- 277 - 296
                currentrecord += PadRight(reportdetails.GenerationCode, ' ', 1);//GenerationCode--------------------------------- 297 - 297
                currentrecord += PadLeft(reportdetails.SSN, '0', 9);//SSN-------------------------------------------------------- 298 - 306
                string dob = "";
                string dobnormal = "";

                if (!string.IsNullOrEmpty(reportdetails.DateofBirth))
                {
                    dob = reportdetails.DateofBirth.Split(' ')[0];
                    if (dob.Length > 7)
                    {
                        dobnormal = dob.Split('/')[0] + dob.Split('/')[1] + dob.Split('/')[2];
                    }
                }
                if (!string.IsNullOrEmpty(dob))
                {
                    if (Convert.ToDateTime(dob).Date != DateTime.MinValue.Date)
                    {
                        totalDOBsforBaseSegment++;
                    }
                }
                currentrecord += PadLeft(dobnormal, '0', 8);//DateofBirth  ------------------------------------------------------ 307 - 314
                currentrecord += PadLeft(reportdetails.TelephoneNumber, '0', 10);//TelephoneNumber ------------------------------ 315 - 324
                currentrecord += PadRight(reportdetails.ECOACode, ' ', 1); //ECOACode ------------------------------------------- 325 - 325
                currentrecord += PadRight(reportdetails.ConsumerInformationIndicator, ' ', 2);//ConsumerInformationIndicator ---- 326 - 327
                currentrecord += PadRight(AdapterConfiguration.CountryCode, ' ', 2); //CountryCode   ---------------------------- 328 - 329
                currentrecord += PadRight(reportdetails.FirstLineOfAddress, ' ', 32);//FirstLineOfAddress      -----------------  330 - 361
                currentrecord += PadRight(reportdetails.SecondLineOfAddress, ' ', 32);//SecondLineOfAddress    -----------------  362 - 393
                // ?????????

                currentrecord += PadRight(reportdetails.City, ' ', 20);//City --------------------------------------------------  394 - 413
                currentrecord += PadRight(reportdetails.State, ' ', 2);//State -------------------------------------------------- 414 - 415
                currentrecord += PadRight(reportdetails.ZipCode, ' ', 9);//ZipCode ---------------------------------------------- 416 - 424
                currentrecord += PadRight(reportdetails.AddressIndicator, ' ', 1);//AddressIndicator ---------------------------- 425 - 425
                currentrecord += PadRight(reportdetails.ResidenceCode, ' ', 1);//ResidenceCode       ---------------------------- 426 - 426

                if (!string.IsNullOrEmpty(reportdetails.TelephoneNumber))
                {
                    if (reportdetails.TelephoneNumber != "0000000000")
                    {
                        totalphonesforBaseSegment++;
                    }
                }

                if (!string.IsNullOrEmpty(reportdetails.SSN))
                {
                    if (reportdetails.SSN.Length == 9 && reportdetails.SSN != "000000000" && reportdetails.SSN != "999999999")
                    {
                        totalSSNsforBaseSegment++;
                    }
                }

                var J1s = item.J1s == null ? null : item.J1s.OrderByDescending(x => x.LastReportDate.Time.DateTime).FirstOrDefault();

                string J11 = "J1";
                string J12 = "J1";
                string J13 = "J1";
                if (J1s != null)
                {
                    if (!string.IsNullOrEmpty(J1s.J11))
                    {
                        J11 = J1s.J11;
                        if (J1s.J11.Trim() != "J1")
                            totalJ1++;
                    }
                    if (!string.IsNullOrEmpty(J1s.J12))
                    {
                        J12 = J1s.J12;
                        if (J1s.J12.Trim() != "J1")
                            totalJ1++;
                    }
                    if (!string.IsNullOrEmpty(J1s.J13))
                    {
                        J13 = J1s.J13;
                        if (J1s.J13.Trim() != "J1")
                            totalJ1++;
                    }
                }

                currentrecord += PadRight(J11, ' ', 100);//J1 ----------------------------------------------------------------------    J1
                currentrecord += PadRight(J12, ' ', 100);//J1 ----------------------------------------------------------------------    J1
                currentrecord += PadRight(J13, ' ', 100);//J1 ----------------------------------------------------------------------    J1

                var J2s = item.J2s == null ? null : item.J2s.OrderByDescending(x => x.LastReportDate.Time.DateTime).FirstOrDefault();

                string J21 = "J2";
                string J22 = "J2";
                string J23 = "J2";
                if (J2s != null)
                {
                    if (!string.IsNullOrEmpty(J2s.J21))
                    {
                        J21 = J2s.J21;
                        if (J2s.J21.Trim() != "J2")
                            totalJ2++;
                    }
                    if (!string.IsNullOrEmpty(J2s.J22))
                    {
                        J22 = J2s.J22;
                        if (J2s.J22.Trim() != "J2")
                            totalJ2++;
                    }
                    if (!string.IsNullOrEmpty(J2s.J23))
                    {
                        J23 = J2s.J23;
                        if (J2s.J23.Trim() != "J2")
                            totalJ2++;
                    }
                }

                currentrecord += PadRight(J21, ' ', 200);//J2 ----------------------------------------------------------------------------- J2
                currentrecord += PadRight(J22, ' ', 200);//J2 ----------------------------------------------------------------------------- J2
                currentrecord += PadRight(J23, ' ', 200);//J2 ----------------------------------------------------------------------------- J2
                //totalSSNsforJ2                

                var kone = item.K1 == null ? new KOne() : item.K1.OrderByDescending(x => x.LastReportDate.Time.DateTime).FirstOrDefault();
                var ktwo = item.K2 == null ? new KTwo() : item.K2.OrderByDescending(x => x.LastReportDate.Time.DateTime).FirstOrDefault();
                var kthree = item.K3 == null ? new KThree() : item.K3.OrderByDescending(x => x.LastReportDate.Time.DateTime).FirstOrDefault();
                var kfour = item.K4 == null ? new KFour() : item.K4.OrderByDescending(x => x.LastReportDate.Time.DateTime).FirstOrDefault();
                var lone = item.L1 == null ? new LOne() : item.L1.OrderByDescending(x => x.LastReportDate.Time.DateTime).FirstOrDefault();
                var none = item.N1 == null ? new NOne() : item.N1.OrderByDescending(x => x.LastReportDate.Time.DateTime).FirstOrDefault();

                currentrecord += PadRight(string.IsNullOrEmpty(kone.K1) ? "K1" : kone.K1, ' ', 34);//k1 ----------------------------------- K1
                //totalOriginalCreditorSegments = 0;

                currentrecord += PadRight(string.IsNullOrEmpty(ktwo.K2) ? "K2" : ktwo.K2, ' ', 34);//K2 ----------------- K2
                //totalPurchasedPortfolioORSoldToSegments = 0;

                currentrecord += PadRight(string.IsNullOrEmpty(kthree.K3) ? "K3" : kthree.K3, ' ', 40);//K3 ----------------- K3
                //totalMortgageInformationSegments = 0;

                currentrecord += PadRight(string.IsNullOrEmpty(kfour.K4) ? "K4" : kfour.K4, ' ', 30);//K4 ----------------- K4
                //totalSpecializedPaymentInformationSegments= 0;

                currentrecord += PadRight(string.IsNullOrEmpty(lone.L1) ? "L1" : lone.L1, ' ', 54);//L1 ----------------- L1
                //totalChangeSegments= 0;

                currentrecord += PadRight(string.IsNullOrEmpty(none.N1) ? "N1" : none.N1, ' ', 146);//N1 ---------------- N1
                totalEmploymentSegments = 0;

                if (reportdetails.AccountStatus == "DA")
                {
                    totalofStatusCodeDA++;
                }
                else if (reportdetails.AccountStatus == "DF")
                {
                    totalofStatusCodeDF++;
                }
                else if (reportdetails.AccountStatus == "05")
                {
                    totalofStatusCode05++;
                }
                else if (reportdetails.AccountStatus == "11")
                {
                    totalofStatusCode11++;
                }
                else if (reportdetails.AccountStatus == "13")
                {
                    totalofStatusCode13++;
                }
                else if (reportdetails.AccountStatus == "61")
                {
                    totalofStatusCode61++;
                }
                else if (reportdetails.AccountStatus == "62")
                {
                    totalofStatusCode62++;
                }
                else if (reportdetails.AccountStatus == "63")
                {
                    totalofStatusCode63++;
                }
                else if (reportdetails.AccountStatus == "64")
                {
                    totalofStatusCode64++;
                }
                else if (reportdetails.AccountStatus == "65")
                {
                    totalofStatusCode65++;
                }
                else if (reportdetails.AccountStatus == "71")
                {
                    totalofStatusCode71++;
                }
                else if (reportdetails.AccountStatus == "78")
                {
                    totalofStatusCode78++;
                }
                else if (reportdetails.AccountStatus == "80")
                {
                    totalofStatusCode80++;
                }
                else if (reportdetails.AccountStatus == "82")
                {
                    totalofStatusCode82++;
                }
                else if (reportdetails.AccountStatus == "83")
                {
                    totalofStatusCode83++;
                }
                else if (reportdetails.AccountStatus == "84")
                {
                    totalofStatusCode84++;
                }
                else if (reportdetails.AccountStatus == "88")
                {
                    totalofStatusCode88++;
                }
                else if (reportdetails.AccountStatus == "89")
                {
                    totalofStatusCode89++;
                }
                else if (reportdetails.AccountStatus == "93")
                {
                    totalofStatusCode93++;
                }
                else if (reportdetails.AccountStatus == "94")
                {
                    totalofStatusCode94++;
                }
                else if (reportdetails.AccountStatus == "95")
                {
                    totalofStatusCode95++;
                }
                else if (reportdetails.AccountStatus == "96")
                {
                    totalofStatusCode96++;
                }
                else if (reportdetails.AccountStatus == "97")
                {
                    totalofStatusCode97++;
                }

                if (reportdetails.ECOACode != "")
                {
                    totalofECOACode++;
                }

                currentrecord = currentrecord.Replace("#LN#", PadLeft(currentrecord.Length.ToString(), '0', 4));

                #endregion

                totalBaseRecords++;
                writer.Write(currentrecord + Environment.NewLine);

                reportdetails.FileId = fileId;

                //await BureauRepository.UpdateFileId(item.LoanNumber, fileId, reportdetails.Id);
                //await BureauRepository.UpdateJOneSegment(item.LoanNumber, J1s);

            }

            totalSSNsforAll = totalSSNsforBaseSegment + totalSSNsforJ1 + totalSSNsforJ2;
            totalDOBsforAll = totalDOBsforBaseSegment + totalDOBsforJ1 + totalDOBsforJ2;
            totalphonesforAll = totalphonesforBaseSegment + totalphonesforJ1 + totalphonesforJ2;

            #region Trailor Record
            {                //-------------------------------------------------------------------         POSITIOM    From -  To
                trailorrecord = "#LN#";          //-----------------------------------------------                       1  -   4
                trailorrecord += "TRAILER";             //-----------------------------------------------                5  -  11

                trailorrecord += PadLeft(totalBaseRecords.ToString(), '0', 9);//                                         12  -  20
                trailorrecord += PadRight("", ' ', 9);   //Reserved-blankfill                                            21  -  29
                trailorrecord += PadLeft(totalofStatusCodeDF.ToString(), '0', 9);//                                      30  -  38
                trailorrecord += PadLeft(totalJ1.ToString(), '0', 9);//                                                  39  -  47
                trailorrecord += PadLeft(totalJ2.ToString(), '0', 9);//                                                  48  -  56
                //?????
                trailorrecord += PadLeft(blockCount.ToString(), '0', 9);//Block count                                                       57  -  65
                trailorrecord += PadLeft(totalofStatusCodeDA.ToString(), '0', 9);//                                      66  -  74
                trailorrecord += PadLeft(totalofStatusCode05.ToString(), '0', 9);//                                      75  -  83
                trailorrecord += PadLeft(totalofStatusCode11.ToString(), '0', 9);//                                      84  -  92
                trailorrecord += PadLeft(totalofStatusCode13.ToString(), '0', 9);//                                      93  - 101
                trailorrecord += PadLeft(totalofStatusCode61.ToString(), '0', 9);//                                     102  - 110
                trailorrecord += PadLeft(totalofStatusCode62.ToString(), '0', 9);//                                     111  - 119
                trailorrecord += PadLeft(totalofStatusCode63.ToString(), '0', 9);//                                     120  - 128
                trailorrecord += PadLeft(totalofStatusCode64.ToString(), '0', 9);//                                     129  - 137
                trailorrecord += PadLeft(totalofStatusCode65.ToString(), '0', 9);//                                     138  - 146
                trailorrecord += PadLeft(totalofStatusCode71.ToString(), '0', 9);//                                     147  - 155
                trailorrecord += PadLeft(totalofStatusCode78.ToString(), '0', 9);//                                     156  - 164
                trailorrecord += PadLeft(totalofStatusCode80.ToString(), '0', 9);//                                     165  - 173
                trailorrecord += PadLeft(totalofStatusCode82.ToString(), '0', 9);//                                     174  - 182
                trailorrecord += PadLeft(totalofStatusCode83.ToString(), '0', 9);//                                     183  - 191
                trailorrecord += PadLeft(totalofStatusCode84.ToString(), '0', 9);//                                     192  - 200
                trailorrecord += PadLeft(totalofStatusCode88.ToString(), '0', 9);//                                     201  - 209
                trailorrecord += PadLeft(totalofStatusCode89.ToString(), '0', 9);//                                     210  - 218
                trailorrecord += PadLeft(totalofStatusCode93.ToString(), '0', 9);//                                     219  - 227
                trailorrecord += PadLeft(totalofStatusCode94.ToString(), '0', 9);//                                     228  - 236
                trailorrecord += PadLeft(totalofStatusCode95.ToString(), '0', 9);//                                     237  - 245
                trailorrecord += PadLeft(totalofStatusCode96.ToString(), '0', 9);//                                     246  - 254
                trailorrecord += PadLeft(totalofStatusCode97.ToString(), '0', 9);//                                     255  - 263
                trailorrecord += PadLeft(totalofECOACode.ToString(), '0', 9);//                                         264  - 272
                trailorrecord += PadLeft(totalEmploymentSegments.ToString(), '0', 9);//                                 273  - 281
                trailorrecord += PadLeft(totalOriginalCreditorSegments.ToString(), '0', 9);//                           282  - 290
                trailorrecord += PadLeft(totalPurchasedPortfolioORSoldToSegments.ToString(), '0', 9);//                 291  - 299
                trailorrecord += PadLeft(totalMortgageInformationSegments.ToString(), '0', 9);//                        300  - 308
                trailorrecord += PadLeft(totalSpecializedPaymentInformationSegments.ToString(), '0', 9);//              309  - 317
                trailorrecord += PadLeft(totalChangeSegments.ToString(), '0', 9);//                                     318  - 326
                trailorrecord += PadLeft(totalSSNsforAll.ToString(), '0', 9);//                                         327  - 335
                trailorrecord += PadLeft(totalSSNsforBaseSegment.ToString(), '0', 9);//                                 336  - 344
                trailorrecord += PadLeft(totalSSNsforJ1.ToString(), '0', 9);//                                          345  - 353
                trailorrecord += PadLeft(totalSSNsforJ2.ToString(), '0', 9);//                                          354  - 362
                trailorrecord += PadLeft(totalDOBsforAll.ToString(), '0', 9);//                                         363  - 371
                trailorrecord += PadLeft(totalDOBsforBaseSegment.ToString(), '0', 9);//                                 372  - 380
                trailorrecord += PadLeft(totalDOBsforJ1.ToString(), '0', 9);//                                          381  - 389
                trailorrecord += PadLeft(totalDOBsforJ2.ToString(), '0', 9);//                                          390  - 398
                trailorrecord += PadLeft(totalphonesforAll.ToString(), '0', 9);//                                       399  - 407
                trailorrecord += PadRight("", ' ', 1257);//                                                             408  - 1664

                trailorrecord = trailorrecord.Replace("#LN#", PadLeft(trailorrecord.Length.ToString(), '0', 4));

            }
            #endregion

            writer.Write(trailorrecord + Environment.NewLine);

            fileinfo.FileSentDate = new TimeBucket(TenantTime.Now);

            FileStorageService.Upload(fs.ToArray(), filename + ".txt", AdapterConfiguration.UploadPath /*"nacha/bureaureport/"*/);

            writer.Close();

            BureauFileRepository.Update(fileinfo);
            return filename;
        }
        private string PadLeft(string inputstring, char pad, int maxlength)
        {
            if (string.IsNullOrEmpty(inputstring))
                inputstring = "";
            if (inputstring.Length > maxlength)
                inputstring = inputstring.Substring(0, maxlength);
            return inputstring.PadLeft(maxlength, pad);
        }
        private string PadRight(string inputstring, char pad, int maxlength)
        {
            if (string.IsNullOrEmpty(inputstring))
                inputstring = "";
            if (inputstring.Length > maxlength)
                inputstring = inputstring.Substring(0, maxlength);
            return inputstring.PadRight(maxlength, pad).ToUpper();
        }

        public async Task<ProductDetails> GetProductParameters(string productId, string loanNumber)
        {
            var productDetails = await DataAttributesEngine.GetAttribute("loan", loanNumber, "product");
            var productObject = new List<ProductDetails>();
            if (productDetails != null)
            {
                productObject = JsonConvert.DeserializeObject<List<ProductDetails>>(productDetails.ToString());
            }
            return productObject.FirstOrDefault();
        }

        public async Task<dynamic> GetBankruptcy(string loanNumber)
        {
            var bankruptcyDetails = await DataAttributesEngine.GetAttribute("loan", loanNumber, "BankruptcyData");
            var productObject = new List<dynamic>();
            if (bankruptcyDetails != null)
            {
                productObject = JsonConvert.DeserializeObject<List<dynamic>>(bankruptcyDetails.ToString());
            }
            return productObject.FirstOrDefault();
        }

        private bool comparestring(string s1, string s2)
        {
            return (string.IsNullOrEmpty(s1) ? "" : s1.ToUpper()) == (string.IsNullOrEmpty(s2) ? "" : s2.ToUpper());
        }

        public async Task<List<IBureauFileResponse>> GetBureauSummaryByLoannumber(string loanNumber)
        {
            var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Bureau Report for {loanNumber} not found");
            }
            List<IBureauFileResponse> filelist = new List<IBureauFileResponse>();
            foreach (var item in loanInformation.BaseSegments)
            {
                var fileinfo = await BureauFileRepository.GetFileByFileId(item.FileId);
                if (fileinfo != null)
                {
                    var fileresponse = new BureauFileResponse
                    {
                        FileId = fileinfo.FileId,
                        FIleName = fileinfo.FileName,
                        FileSentDate = fileinfo.FileSentDate,
                        LastReportedDate = item.LastReportDate
                    };
                    filelist.Add(fileresponse);
                }

            }

            return filelist;
        }

        public async Task<IBaseSegment> GetBaseSegment(string loanNumber, string fileId)
        {
            var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Bureau Report for {loanNumber} not found");
            }

            if (loanInformation.BaseSegments != null)
            {
                return loanInformation.BaseSegments.Where(i => i.FileId == fileId).FirstOrDefault();
            }
            else return null;
        }

        //public async Task<IBureauMaster> GetAllSegment(string loanNumber, string fileId)
        //{
        //    var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
        //    if (loanInformation == null)
        //    {
        //        throw new NotFoundException($"Bureau Report for {loanNumber} not found");
        //    }

        //    IBureauMaster bureauMaster = new BureauMaster();
        //    if (loanInformation.BaseSegments != null)
        //    {
        //        var basesegment=loanInformation.BaseSegments.Where(i => i.FileId == fileId).FirstOrDefault();

        //        bureauMaster.BaseSegments.Add(basesegment);
        //    }

        //    if (loanInformation.J1s != null)
        //    {
        //        var J1segment = loanInformation.J1s.Where(i => i.FileId == fileId).FirstOrDefault();

        //        bureauMaster.J1s.Add(J1segment);
        //    }

        //    if (loanInformation.J2s != null)
        //    {
        //        var J2segment = loanInformation.J2s.Where(i => i.FileId == fileId).FirstOrDefault();

        //        bureauMaster.J2s.Add(J2segment);
        //    }

        //    if (loanInformation.J2s != null)
        //    {
        //        var J2segment = loanInformation.J2s.Where(i => i.FileId == fileId).FirstOrDefault();

        //        bureauMaster.J2s.Add(J2segment);
        //    }

        //}

        public async Task<IJOne> GetJOneSegment(string loanNumber, string fileId)
        {
            var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Bureau Report for {loanNumber} and {fileId} not found");
            }

            if (loanInformation.J1s != null)
            {
                return loanInformation.J1s.Where(i => i.FileId == fileId).FirstOrDefault();
            }
            else return null;
        }

        public async Task<IJTwo> GetJTwoSegment(string loanNumber, string fileId)
        {
            var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Bureau Report for {loanNumber} and {fileId} not found");
            }

            if (loanInformation.J2s != null)
            {
                return loanInformation.J2s.Where(i => i.FileId == fileId).FirstOrDefault();
            }
            else return null;
        }

        public async Task<IKOne> GetKOneSegment(string loanNumber, string fileId)
        {
            var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Bureau Report for {loanNumber} and {fileId} not found");
            }

            if (loanInformation.K1 != null)
            {
                return loanInformation.K1.Where(i => i.FileId == fileId).FirstOrDefault();
            }
            else return null;
        }

        public async Task<IKTwo> GetKTwoSegment(string loanNumber, string fileId)
        {
            var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Bureau Report for {loanNumber} and {fileId} not found");
            }

            if (loanInformation.K2 != null)
            {
                return loanInformation.K2.Where(i => i.FileId == fileId).FirstOrDefault();
            }
            else return null;
        }

        public async Task<IKThree> GetKThreeSegment(string loanNumber, string fileId)
        {
            var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Bureau Report for {loanNumber} and {fileId} not found");
            }

            if (loanInformation.K3 != null)
            {
                return loanInformation.K3.Where(i => i.FileId == fileId).FirstOrDefault();
            }
            else return null;
        }

        public async Task<IKFour> GetKFourSegment(string loanNumber, string fileId)
        {
            var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Bureau Report for {loanNumber} and {fileId} not found");
            }

            if (loanInformation.K4 != null)
            {
                return loanInformation.K4.Where(i => i.FileId == fileId).FirstOrDefault();
            }
            else return null;
        }

        public async Task<ILOne> GetLOneSegment(string loanNumber, string fileId)
        {
            var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Bureau Report for {loanNumber} and {fileId} not found");
            }

            if (loanInformation.L1 != null)
            {
                return loanInformation.L1.Where(i => i.FileId == fileId).FirstOrDefault();
            }
            else return null;
        }

        public async Task<INOne> GetNOneSegment(string loanNumber, string fileId)
        {
            var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Bureau Report for {loanNumber} and {fileId} not found");
            }

            if (loanInformation.N1 != null)
            {
                return loanInformation.N1.Where(i => i.FileId == fileId).FirstOrDefault();
            }
            else return null;
        }

        public async Task<IBureauMaster> GetAllSegment(string loanNumber, string fileId)
        {
            var loanInformation = await BureauRepository.GetLoanInformationByLoanNumber(loanNumber);
            if (loanInformation == null)
            {
                throw new NotFoundException($"Bureau Report for {loanNumber} not found");
            }
            var loandetails = new BureauMaster();
            loandetails.BaseSegments = new List<IBaseSegment>();
            loandetails.J1s = new List<IJOne>();
            loandetails.J2s = new List<IJTwo>();
            loandetails.K1 = new List<IKOne>();
            loandetails.K2 = new List<IKTwo>();
            loandetails.K3 = new List<IKThree>();
            loandetails.K4 = new List<IKFour>();
            loandetails.L1 = new List<ILOne>();
            loandetails.N1 = new List<INOne>();
            loandetails.LoanNumber = loanNumber;
            loandetails.BureauCreateDate = loanInformation.BureauCreateDate;
            if (loanInformation.BaseSegments != null)
            {
                var basesegment = loanInformation.BaseSegments.Where(i => i.FileId == fileId).FirstOrDefault();
                loandetails.BaseSegments.Add(basesegment);
            }
            if (loanInformation.J1s != null)
            {
                var j1s = loanInformation.J1s.Where(i => i.FileId == fileId).FirstOrDefault();
                loandetails.J1s.Add(j1s);
            }
            if (loanInformation.J2s != null)
            {
                var j2s = loanInformation.J2s.Where(i => i.FileId == fileId).FirstOrDefault();
                loandetails.J2s.Add(j2s);
            }
            if (loanInformation.K1 != null)
            {
                var k1 = loanInformation.K1.Where(i => i.FileId == fileId).FirstOrDefault();
                loandetails.K1.Add(k1);
            }
            if (loanInformation.K2 != null)
            {
                var k2 = loanInformation.K2.Where(i => i.FileId == fileId).FirstOrDefault();
                loandetails.K2.Add(k2);
            }
            if (loanInformation.K3 != null)
            {
                var k3 = loanInformation.K3.Where(i => i.FileId == fileId).FirstOrDefault();
                loandetails.K3.Add(k3);
            }
            if (loanInformation.K4 != null)
            {
                var k4 = loanInformation.K4.Where(i => i.FileId == fileId).FirstOrDefault();
                loandetails.K4.Add(k4);
            }
            if (loanInformation.N1 != null)
            {
                var N1 = loanInformation.N1.Where(i => i.FileId == fileId).FirstOrDefault();
                loandetails.N1.Add(N1);
            }
            if (loanInformation.L1 != null)
            {
                var L1 = loanInformation.L1.Where(i => i.FileId == fileId).FirstOrDefault();
                loandetails.L1.Add(L1);
            }

            return loandetails;
        }

        public async Task<List<IBureauFile>> GetAllFiles()
        {
            return await BureauFileRepository.GetFileList();
        }

        public async Task<List<string>> GetFileDetailsByFileId(string fileId)
        {
            return await BureauRepository.GetLoanListByFileId(fileId);

        }
        public async Task<IBaseSegment> UpdateBaseSegment(string loanNumber, BaseSegmentRequest basesegment)
        {
            var loanInformation = await BureauRepository.UpdateBaseSegment(loanNumber, basesegment);
            return loanInformation;
        }

        public async Task<IJOne> UpdateJOneSegment(string loanNumber, JOneRequest J1)
        {
            var loanInformation = await BureauRepository.UpdateJOneSegment(loanNumber, J1);
            return loanInformation;
        }

        public async Task<IJTwo> UpdateJTwoSegment(string loanNumber, JTwoRequest J2)
        {
            var loanInformation = await BureauRepository.UpdateJTwoSegment(loanNumber, J2);
            return loanInformation;
        }

        public async Task<IKOne> UpdateKOneSegment(string loanNumber, KOneRequest K1)
        {
            var loanInformation = await BureauRepository.UpdateKOneSegment(loanNumber, K1);
            return loanInformation;
        }

        public async Task<IKTwo> UpdateKTwoSegment(string loanNumber, KTwoRequest K2)
        {
            var loanInformation = await BureauRepository.UpdateKTwoSegment(loanNumber, K2);
            return loanInformation;
        }

        public async Task<IKThree> UpdateKThreeSegment(string loanNumber, KThreeRequest K3)
        {
            var loanInformation = await BureauRepository.UpdateKThreeSegment(loanNumber, K3);
            return loanInformation;
        }

        public async Task<IKFour> UpdateKFourSegment(string loanNumber, KFourRequest K4)
        {
            var loanInformation = await BureauRepository.UpdateKFourSegment(loanNumber, K4);
            return loanInformation;
        }

        public async Task<ILOne> UpdateLOneSegment(string loanNumber, LOneRequest L1)
        {
            var loanInformation = await BureauRepository.UpdateLOneSegment(loanNumber, L1);
            return loanInformation;
        }

        public async Task<INOne> UpdateNOneSegment(string loanNumber, NOneRequest N1)
        {
            var loanInformation = await BureauRepository.UpdateNOneSegment(loanNumber, N1);
            return loanInformation;
        }

        public async Task<IBaseSegment> UpdateMultipleLoans(MultipleUpdateRequest requestdata)
        {
            var loanInformation = await BureauRepository.UpdateMultipleColumn(requestdata.UpdateDetails, requestdata.ColumnName, requestdata.ColumnValue);
            return loanInformation;
        }


        //private string ExportFileToBureau(ref List<FedChexDataView> batchRecords, ref List<IApplicationFunding> fundingDatails,
        //  DateTimeOffset tenantTime,
        //  string merchantId, string fileTypeNumericValue,
        //  Dictionary<string, string> csvProjection,
        //  string fedChexFileStoragePath,string fileName)
        //{
        //    var txtMemoryStream = new MemoryStream();
        //    var csvGeneratedFileName = fileName;

        //    string CsvDate = tenantTime.ToString("yyyy") + tenantTime.ToString("MM") + tenantTime.ToString("dd");

        //    var head = new List<Header>();
        //    head.Add(new Header { CsvHeader = "HEADER", CsvHeaderDate = CsvDate, ClientNumericValue = fileTypeNumericValue });

        //    var csvHeaderStream = new MemoryStream(CsvGenerator.WriteToCsv(head, csvProjection));
        //    csvHeaderStream.CopyTo(csvMemoryStream);

        //    var csvRecordsStream = new MemoryStream(CsvGenerator.WriteToCsv(batchRecords, csvProjection));
        //    csvRecordsStream.CopyTo(csvMemoryStream);

        //    var totalAmount = fundingDatails.Sum(x => x.AmountFunded);

        //    var csvTailer = new List<Tailer>();
        //    csvTailer.Add(new Tailer
        //    {
        //        ClientNumericValue = fileTypeNumericValue,
        //        CsvTailer = "TRAILER",
        //        CsvTailerDate = CsvDate,
        //        TotalAmount = totalAmount.ToString("0.00").Replace(".", ""),
        //        TotalRecordCount = batchRecords.Count()
        //    });

        //    var csvTailerStream = new MemoryStream(CsvGenerator.WriteToCsv(csvTailer, csvProjection));
        //    csvTailerStream.CopyTo(csvMemoryStream);

        //    using (var csvFileStream = new MemoryStream(csvMemoryStream.ToArray()))
        //    {
        //        FileStorageService.Upload(csvFileStream, csvGeneratedFileName, fedChexFileStoragePath);
        //    }
        //    return csvGeneratedFileName;
        //}
    }
}
