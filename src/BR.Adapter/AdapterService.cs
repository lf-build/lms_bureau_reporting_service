﻿using LendFoundry.Foundation.Date;
using System.Threading.Tasks;
using LendFoundry.NumberGenerator;
using CreditExchange.StatusManagement;
using System.Collections.Generic;
using System.Linq;
using System;
using LendFoundry.Foundation.Services;
using LendFoundry.EventHub.Client;
using LendFoundry.ProductConfiguration;
using BR.Adapter.Abstractions;
using LendFoundry.Foundation.Lookup;
using System.Dynamic;
using LendFoundry.DataAttributes;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using LMS.Loan.Filters.Abstractions;


namespace BR.Adapter
{
    public class AdapterService : IAdapterService
    {
        #region Constructor

        public AdapterService
        (
           AdapterConfiguration adapterConfiguration,
           ILoanScheduleService loanScheduleService,
           ILoanOnboardingService loanOnboardingService,
           IAccrualBalanceService accrualBalanceService,
           //IGeneratorService numberGeneratorService,
           IEntityStatusService statusManagementService,
           IProductService productService,
           //IEventHubClient eventHubClient,
           ITenantTime tenantTime,
           //ILookupService lookup,
           IDataAttributesEngine dataAttributesEngine,
           IAdapterRepository adapterRepository,
           ILoanFilterService loanFilterService,
           BR.Adapter.Abstractions.IPaymentRepository paymentRepository,
           IScheduleRepository scheduleRepository,
           IAccrualDetailsRepository accrualDetailsRepository
        )
        {
            if (adapterConfiguration == null)
                throw new ArgumentException("Configuration not set");

            LoanConfiguration = adapterConfiguration;
            LoanScheduleService = loanScheduleService;
            //NumberGeneratorService = numberGeneratorService;
            ProductService = productService;
            StatusManagementService = statusManagementService;
            //EventHubClient = eventHubClient;
            TenantTime = tenantTime;
            //Lookup = lookup;
            DataAttributesEngine = dataAttributesEngine;
            AccrualBalanceService = accrualBalanceService;
            AdapterRepository = adapterRepository;
            LoanOnboardingService = loanOnboardingService;
            LoanFilterService = loanFilterService;
            PaymentRepository = paymentRepository;
            ScheduleRepository = scheduleRepository;
            AccrualDetailsRepository = accrualDetailsRepository;
        }

        #endregion

        #region Variables
        private IAccrualBalanceService AccrualBalanceService { get; set; }
        private AdapterConfiguration LoanConfiguration { get; }
        private ILoanScheduleService LoanScheduleService { get; }
        private ILoanOnboardingService LoanOnboardingService { get; }
        private ILoanFilterService LoanFilterService { get; }
        private ITenantTime TenantTime { get; }
        private IAdapterRepository AdapterRepository { get; }
        private IScheduleRepository ScheduleRepository { get; }
        private IAccrualDetailsRepository AccrualDetailsRepository { get; }
        private IGeneratorService NumberGeneratorService { get; }
        private IEventHubClient EventHubClient { get; }
        private IEntityStatusService StatusManagementService { get; }
        private IProductService ProductService { get; }
        private ILookupService Lookup { get; }
        private IDataAttributesEngine DataAttributesEngine { get; }
        private BR.Adapter.Abstractions.IPaymentRepository PaymentRepository { get; }

        #endregion


        public async Task<string> ExctractLoanInformation()
        {
            // var loans = await LoanFilterService.GetAllByStatus(new System.Collections.Generic.List<string> { "200.30" });

            await AccrualDetailsRepository.RemoveHistory();
            await ScheduleRepository.RemoveHistory();
            await PaymentRepository.RemoveHistory();
            await AdapterRepository.RemoveHistory();

            // get all 
            var loans = await LoanFilterService.GetAllByStatus(new System.Collections.Generic.List<string> { "200.10", "200.20", "200.30", "200.40", "200.50", "200.60", "200.70", "200.80", "200.90", "200.11", "200.12" });
            foreach (var item in loans.OrderByDescending(x => x.LoanNumber))
            {
                var loanInformation = await LoanOnboardingService.GetLoanInformationByLoanNumber(item.LoanNumber);

                BR.Adapter.Abstractions.ILoanInformation loanInfo = new BR.Adapter.Abstractions.LoanInformation
                {

                    LoanNumber = loanInformation.LoanNumber,
                    LoanFundedDate = loanInformation.LoanFundedDate,
                    LoanStartDate = loanInformation.LoanStartDate,
                    LoanEndDate = loanInformation.LoanEndDate,
                    ProductPortfolioType = item.ProductPortfolioType,
                    AccrualStop = item.AccrualStop,
                    AccrualStopDate = item.AccrualStopDate,
                    AnnualRate = item.AnnualRate,
                    AutoPayStartDate = item.AutoPayStartDate,
                    AutoPayStopDate = item.AutoPayStopDate,
                    ChargeOff = item.ChargeOff,
                    CurrentLoanScheduleNumber = item.CurrentLoanScheduleNumber,
                    CurrentLoanScheduleVersion = item.CurrentLoanScheduleVersion,
                    DailyRate = item.DailyRate,
                    Due = item.Due,
                    ExcessMoney = item.ExcessMoney,
                    FactorRate = item.FactorRate,
                    FirstPaymentDate = item.FirstPaymentDate,
                    FrequencyRate = item.FrequencyRate,
                    Fundedamount = item.Fundedamount,
                    FundedDate = item.FundedDate,
                    LoanAmount = item.LoanAmount,
                    IsAutoPay = item.IsAutoPay,
                    LoanApplicationNumber = item.LoanApplicationNumber,
                    LoanOnboardedDate = item.LoanOnboardedDate,
                    LoanProductId = item.LoanProductId,
                    PaymentDetails = item.PaymentDetails,
                    PaymentFrequency = item.PaymentFrequency,
                    PaymentInfo = item.PaymentInfo,
                    PayOff = item.PayOff,
                    PBOC = item.PBOC,
                    PBOT = item.PBOT,
                    PrimaryApplicantAddressLine1 = item.PrimaryApplicantAddressLine1,
                    PrimaryApplicantAddressLine2 = item.PrimaryApplicantAddressLine2,
                    PrimaryApplicantCity = item.PrimaryApplicantCity,
                    PrimaryApplicantDOB = item.PrimaryApplicantDOB,
                    PrimaryApplicantEmailAddress = item.PrimaryApplicantEmailAddress,
                    PrimaryApplicantFirstName = item.PrimaryApplicantFirstName,
                    PrimaryApplicantGeneration = item.PrimaryApplicantGeneration,
                    PrimaryApplicantLastName = item.PrimaryApplicantLastName,
                    PrimaryApplicantMiddle = item.PrimaryApplicantMiddle,
                    PrimaryApplicantPhoneNo = item.PrimaryApplicantPhoneNo,
                    PrimaryApplicantPositionOfLoan = item.PrimaryApplicantPositionOfLoan,
                    PrimaryApplicantRole = item.PrimaryApplicantRole,
                    PrimaryApplicantSalutation = item.PrimaryApplicantSalutation,
                    PrimaryApplicantSSN = item.PrimaryApplicantSSN,
                    PrimaryApplicantState = item.PrimaryApplicantState,
                    PrimaryApplicantZip = item.PrimaryApplicantZip,
                    PrimaryBusinessAddressLine1 = item.PrimaryBusinessAddressLine1,
                    PrimaryBusinessAddressLine2 = item.PrimaryBusinessAddressLine2,
                    PrimaryBusinessCity = item.PrimaryBusinessCity,
                    PrimaryBusinessDBA = item.PrimaryBusinessDBA,
                    PrimaryBusinessEmailAddress = item.PrimaryBusinessEmailAddress,
                    PrimaryBusinessFedTaxId = item.PrimaryBusinessFedTaxId,
                    PrimaryBusinessName = item.PrimaryBusinessName,
                    PrimaryBusinessPhoneNo = item.PrimaryBusinessPhoneNo,
                    PrimaryBusinessState = item.PrimaryBusinessState,
                    PrimaryBusinessZip = item.PrimaryBusinessZip,
                    PrincipalAmount = item.PrincipalAmount,
                    ProcessingDate = item.ProcessingDate,
                    ProductCategory = item.ProductCategory,
                    Reasons = item.Reasons,
                    SellOff = item.SellOff,
                    StatusCode = item.StatusCode,
                    StatusDate = item.StatusDate,
                    StatusName = item.StatusName,
                    StatusWorkFlowId = item.StatusWorkFlowId,
                    SubStatusDetail = item.SubStatusDetail,
                    Tenure = item.Tenure,
                    WorkFlowStatus = item.WorkFlowStatus,
                    PaymentInstruments = loanInformation.PaymentInstruments,
                    ApplicantDetails = loanInformation.ApplicantDetails,
                    BusinessDetails = loanInformation.BusinessDetails,
                    DrawDownDetails = loanInformation.DrawDownDetails,
                    Currency = loanInformation.Currency,
                    OtherContacts = loanInformation.OtherContacts,
                    CreditLimit = item.CreditLimit

                };

                AdapterRepository.Add(loanInfo);

                var paymentInformation = await AccrualBalanceService.GetPaymentDetail(loanInfo.LoanNumber);

                foreach (var paymentitem in paymentInformation)
                {
                    PaymentRepository.Add(paymentitem);
                }

                var activeSchedule = await LoanScheduleService.GetLoanSchedule(item.LoanNumber);

                var scheduleDetails = await AccrualBalanceService.GetPaidDetail(item.LoanNumber, activeSchedule.ScheduleVersionNo);

                if (scheduleDetails.Count() > 0)
                {

                    BR.Adapter.Abstractions.ILoanSchedules scheduleinfo = new BR.Adapter.Abstractions.LoanSchedules
                    {
                        LoanNumber = item.LoanNumber,
                        ScheduledCreatedReason = activeSchedule.ScheduledCreatedReason,
                        ModReason = activeSchedule.ModReason,
                        Scheduledetails = scheduleDetails
                    };

                    ScheduleRepository.Add(scheduleinfo);
                }

                var acrualInformation = await AccrualBalanceService.GetLoanByScheduleVersion(loanInfo.LoanNumber, activeSchedule.ScheduleVersionNo);
                if (acrualInformation != null)
                {
                    var accrualinfo = new AccrualDetails
                    {
                        LoanNumber = acrualInformation.LoanNumber,
                        AccrualStop = acrualInformation.AccrualStop,
                        AccrualStopDate = acrualInformation.AccrualStopDate,
                        ChargeOff = acrualInformation.ChargeOff,
                        DailyRate = acrualInformation.DailyRate,
                        Due = acrualInformation.Due,
                        ExcessMoney = acrualInformation.ExcessMoney,
                        InstallmentAmount = acrualInformation.InstallmentAmount,
                        PaymentInfo = acrualInformation.PaymentInfo,
                        PayOff = acrualInformation.PayOff,
                        PBOC = acrualInformation.PBOC,
                        PBOT = acrualInformation.PBOT,
                        PrincipalAmount = acrualInformation.PrincipalAmount,
                        ProcessingDate = acrualInformation.ProcessingDate,
                        ProductCategory = acrualInformation.ProductCategory,
                        ProductId = acrualInformation.ProductId,
                        ProductPortfolioType = acrualInformation.ProductPortfolioType,
                        Schedule = acrualInformation.Schedule,
                        ScheduleVersion = acrualInformation.ScheduleVersion,
                        SellOff = acrualInformation.SellOff,
                        StartDate = acrualInformation.StartDate
                    };
                    AccrualDetailsRepository.Add(accrualinfo);
                }


            }
            return "";
        }

        private string PadLeft(string inputstring, char pad, int maxlength)
        {
            return inputstring.PadLeft(maxlength, pad);
        }

        private string PadRight(string inputstring, char pad, int maxlength)
        {
            return inputstring.PadRight(maxlength, pad);
        }

    }
}
