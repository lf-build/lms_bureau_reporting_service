﻿using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.NumberGenerator.Client;
using LendFoundry.EventHub.Client;
using CreditExchange.StatusManagement.Client;
using LendFoundry.ProductConfiguration.Client;
using BR.Adapter.Abstractions;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.DataAttributes.Client;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using LMS.Loan.Filters.Abstractions;


namespace BR.Adapter
{
    public class AdapterServiceFactory : IAdapterServiceFactory
    {
        public AdapterServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public IAdapterService Create(ITokenReader reader, ITokenHandler handler, ILogger logger)
        {
            var adapterRepositoryService = Provider.GetService<IAdapterRepositoryFactory>();
            var adapterRepository = adapterRepositoryService.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();


            var adapterConfigurationService = configurationServiceFactory.Create<AdapterConfiguration>(Abstractions.Settings.ServiceName, reader);
            var adapterConfiguration = adapterConfigurationService.Get();

            var numberGeneratorServiceFactory = Provider.GetService<INumberGeneratorServiceFactory>();
            var numberGeneratorService = numberGeneratorServiceFactory.Create(reader);

            var loanScheduleServiceFactory = Provider.GetService<ILoanScheduleServiceFactory>();
            var loanScheduleService = loanScheduleServiceFactory.Create(reader, handler, logger);

            var loanOnboardingServiceFactory = Provider.GetService<ILoanOnboardingServiceFactory>();
            var loanOnboardingService = loanOnboardingServiceFactory.Create(reader, handler, logger);

            var loanFilterServiceFactory = Provider.GetService<ILoanFilterServiceFactory>();
            var loanFilterService = loanFilterServiceFactory.Create(reader, logger, handler);


            //var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            //var eventHub = eventHubFactory.Create(reader);

            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory>();
            var statusManagementService = statusManagementServiceFactory.Create(reader);

            var productServiceFactory = Provider.GetService<IProductServiceFactory>();
            var productService = productServiceFactory.Create(reader);

            var loanOnboardingHistoryRepositoryFactory = Provider.GetService<ILoanOnboardingHistoryRepositoryFactory>();
            var loanOnboardingHistoryRepository = loanOnboardingHistoryRepositoryFactory.Create(reader);

            //var lookupServiceFactory = Provider.GetService<ILookupClientFactory>();
            //var lookupService = lookupServiceFactory.Create(reader);

            var dataAttributesServiceFactory = Provider.GetService<IDataAttributesClientFactory>();
            var dataAttributesService = dataAttributesServiceFactory.Create(reader);

            var accrualServiceFactory = Provider.GetService<IAccrualBalanceClientFactory>();
            var accrualService = accrualServiceFactory.Create(reader);

            var loanOnboardingRepositoryService = Provider.GetService<ILoanOnboardingRepositoryFactory>();
            var loanOnboardingRepository = loanOnboardingRepositoryService.Create(reader);

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var paymentRepositoryService = Provider.GetService<IPaymentRepositoryFactory>();
            var paymentRepository = paymentRepositoryService.Create(reader);

            var scheduleRepositoryService = Provider.GetService<IScheduleRepositoryFactory>();
            var scheduleRepository = scheduleRepositoryService.Create(reader);

            var accrualdetailsRepositoryService = Provider.GetService<IAccrualDetailsRepositoryFactory>();
            var accrualdetailsRepository = accrualdetailsRepositoryService.Create(reader);

            return new AdapterService(adapterConfiguration, loanScheduleService, loanOnboardingService, accrualService, statusManagementService, productService, /*eventHub, */tenantTime, /*lookupService,*/ dataAttributesService, adapterRepository, loanFilterService, paymentRepository, scheduleRepository, accrualdetailsRepository);
        }
    }
}
