﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public class MapAccountDetails
    {
        public string AccountStatus { get; set; }
        public string PaymentRating { get; set; }
        public string PaymentProfile { get; set; }

    }
}
