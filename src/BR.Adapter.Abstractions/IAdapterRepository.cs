﻿using LendFoundry.Foundation.Persistence;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IAdapterRepository : IRepository<ILoanInformation>
    {
        Task<List<ILoanInformation>> GetLoansList();

        Task<DeleteResult> RemoveHistory();
    }
}