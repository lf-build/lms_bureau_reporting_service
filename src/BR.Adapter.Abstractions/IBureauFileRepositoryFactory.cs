﻿using LendFoundry.Security.Tokens;

namespace BR.Adapter.Abstractions
{
    public interface IBureauFileRepositoryFactory
    {
        IBureauFileRepository Create(ITokenReader reader);
    }
}