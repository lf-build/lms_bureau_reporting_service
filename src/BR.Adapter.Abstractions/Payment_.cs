﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public class Payment_ : Aggregate, IPayment_
    {
        public string LoanNumber { get; set; }
        public string ScheduleVersion { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Description { get; set; }
        public double Principal { get; set; }
        public double FeeAmount { get; set; }
        public double Interest { get; set; }
        public double AdditionalInterest { get; set; }
        public double CumulativeAdditionalInterestPaid { get; set; }
        public double AmountPaid { get; set; }

        public double CumulativePrincipalPaid { get; set; }

        public double CumulativeFeePaid { get; set; }

        public double CumulativeInterestPaid { get; set; }

        public double CumulativeTotalPaid { get; set; }

        public DateTime CreatedDate { get; set; }




    }
}
