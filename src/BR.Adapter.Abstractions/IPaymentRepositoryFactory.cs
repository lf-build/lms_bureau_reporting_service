﻿using LendFoundry.Security.Tokens;

namespace BR.Adapter.Abstractions
{
    public interface IPaymentRepositoryFactory
    {
        IPaymentRepository Create(ITokenReader reader);
    }
}