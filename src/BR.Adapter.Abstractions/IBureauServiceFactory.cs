﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LMS.Foundation.Amortization;

namespace BR.Adapter.Abstractions
{
    public interface IBureauServiceFactory
    {
        IBureauService Create(ITokenReader reader, ITokenHandler handler, ILogger logger);
    }
}
