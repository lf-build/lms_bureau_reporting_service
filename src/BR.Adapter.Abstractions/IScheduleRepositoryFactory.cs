﻿using LendFoundry.Security.Tokens;

namespace BR.Adapter.Abstractions
{
    public interface IScheduleRepositoryFactory
    {
        IScheduleRepository Create(ITokenReader reader);
    }
}