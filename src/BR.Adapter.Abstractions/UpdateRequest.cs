﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public class UpdateRequest : IUpdateRequest
    {
        public UpdateRequest()
        {

        }
        public string LoanNumber { get; set; }

        public string ReportId { get; set; }
    }
}
