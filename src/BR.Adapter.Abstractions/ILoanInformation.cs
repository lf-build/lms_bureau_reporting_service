﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface ILoanInformation : LMS.Loan.Filters.IFilterView, IAggregate
    {
        List<LMS.LoanManagement.Abstractions.IPaymentInstrument> PaymentInstruments { get; set; }
        [JsonConverter(typeof(InterfaceConverter<LMS.LoanManagement.Abstractions.IApplicantDetails, LMS.LoanManagement.Abstractions.ApplicantDetails>))]
        List<LMS.LoanManagement.Abstractions.IApplicantDetails> ApplicantDetails { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<LMS.LoanManagement.Abstractions.IBusinessDetails, LMS.LoanManagement.Abstractions.BusinessDetails>))]
        List<LMS.LoanManagement.Abstractions.IBusinessDetails> BusinessDetails { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<LMS.LoanManagement.Abstractions.IDrawDownDetails, LMS.LoanManagement.Abstractions.DrawDownDetails>))]
        List<LMS.LoanManagement.Abstractions.IDrawDownDetails> DrawDownDetails { get; set; }
        LMS.LoanManagement.Abstractions.Currency Currency { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<LMS.LoanManagement.Abstractions.IOtherContact, LMS.LoanManagement.Abstractions.OtherContact>))]
        List<LMS.LoanManagement.Abstractions.IOtherContact> OtherContacts { get; set; }



    }
}
