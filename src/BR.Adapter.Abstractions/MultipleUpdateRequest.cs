﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public class MultipleUpdateRequest : IMultipleUpdateRequest
    {
        public MultipleUpdateRequest()
        {

        }

        [JsonConverter(typeof(InterfaceListConverter<IUpdateRequest, UpdateRequest>))]
        public List<IUpdateRequest> UpdateDetails { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }

    }
}
