﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IBureauMaster : IAggregate
    {

        string LoanNumber { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IBaseSegment, BaseSegment>))]
        List<IBaseSegment> BaseSegments { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IJOne, JOne>))]
        List<IJOne> J1s { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IJTwo, JTwo>))]
        List<IJTwo> J2s { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IKOne, KOne>))]
        List<IKOne> K1 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IKTwo, KTwo>))]
        List<IKTwo> K2 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IKThree, KThree>))]
        List<IKThree> K3 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IKFour, KFour>))]
        List<IKFour> K4 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILOne, LOne>))]
        List<ILOne> L1 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<INOne, NOne>))]
        List<INOne> N1 { get; set; }
        TimeBucket BureauCreateDate { get; set; } //DateBougthInBureau - first time loan added to bureau

    }
}
