﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public class NOne : INOne
    {
        public string FileId { get; set; }
        public string Id { get; set; }
        public string N1 { get; set; }
        public TimeBucket LastReportDate { get; set; }
    }
}
