﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IBureauFile : IAggregate
    {
        TimeBucket FileSentDate { get; set; }
        string FileName { get; set; }
        string FileId { get; set; }
        TimeBucket CreatedDate { get; set; }

    }
}
