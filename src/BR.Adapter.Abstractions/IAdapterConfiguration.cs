﻿using LendFoundry.Security.Identity;

namespace BR.Adapter.Abstractions
{
    public interface IAdapterConfiguration
    {

        string InnovisProgramIdentifier { get; set; }
        string EquifaxProgramIdentifier { get; set; }
        string ExperianProgramIdentifier { get; set; }
        string TransUnionProgramIdentifier { get; set; }
        string ReporterName { get; set; }
        string ReporterAddress { get; set; }
        string ReporterTelephoneNumber { get; set; }
        string SoftwareVendorName { get; set; }
        string SoftwareVersionNumber { get; set; }
        string AccountType { get; set; }
        string ECOACode { get; set; }
        string CountryCode { get; set; }
        string InterestRateIndicator { get; set; }
        string IdentificationNumber { get; set; }
        string ProcessingIndicator { get; set; }

        string Host { get; set; }
        int Port { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        string SshHostKey { get; set; }


    }
}
