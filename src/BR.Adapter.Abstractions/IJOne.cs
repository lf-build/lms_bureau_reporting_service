﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IJOne
    {
        string Id { get; set; }
        string FileId { get; set; }
        string J11 { get; set; }
        string J12 { get; set; }
        string J13 { get; set; }
        TimeBucket LastReportDate { get; set; }
    }
}
