﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace BR.Adapter.Abstractions
{
    public class Settings
    {
        public static string ServiceName { get; } = "br-adapter";
        private static string Prefix { get; } = ServiceName.ToUpper();

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        //public static ServiceSettings NumberGenerator { get; } = new ServiceSettings($"{Prefix}_NUMBER-GENERATOR", "number-generator");

        //public static ServiceSettings Lookup { get; } = new ServiceSettings($"{Prefix}_LOOKUP-SERVICE", "lookup-service");

        public static ServiceSettings StatusManagement { get; } = new ServiceSettings($"{Prefix}_STATUS-MANAGEMENT", "status-management");

        public static ServiceSettings LoanAccounting { get; } = new ServiceSettings($"{Prefix}_LOAN-ACCOUNTING", "loan-accounting");

        public static ServiceSettings Product { get; } = new ServiceSettings($"{Prefix}_PRODUCT", "product");

        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATA-ATTRIBUTES", "data-attributes");

        public static ServiceSettings ProductRule { get; } = new ServiceSettings($"{Prefix}_PRODUCT-RULE", "productrule");

        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISION-ENGINE", "decision-engine");

        public static ServiceSettings Calendar { get; } = new ServiceSettings($"{Prefix}_CALENDAR", "calendar");
        public static ServiceSettings LoanManagement { get; } = new ServiceSettings($"{Prefix}_LOAN-MANAGEMENT", "loan-management");
        public static ServiceSettings LoanFilters { get; } = new ServiceSettings($"{Prefix}_LOAN-FILTERS", "loan-filters");
        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";
        
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", ServiceName);
    }
}
