﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public class BaseSegmentRequest : IBaseSegmentRequest
    {

        public BaseSegmentRequest()
        {

        }
        public string Id { get; set; }
        public string FileId { get; set; }
        public string ConsumerAccountNumber { get; set; }
        public string PortfolioType { get; set; }
        public string AccountType { get; set; }
        public string CorrectionIndicator { get; set; }
        public string CycleIdentifier { get; set; }
        public string InterestTypeIndicator { get; set; }
        public DateTime DateOpened { get; set; }
        public double CreditLimit { get; set; }
        public double HighestCredit { get; set; }
        public int TermsDuration { get; set; }
        public string TermsFrequency { get; set; }
        public double ScheduledMonthlyPaymentAmount { get; set; }
        public double ActualPaymentAmount { get; set; }
        public string AccountStatus { get; set; }
        public string PaymentRating { get; set; }
        public string PaymentHistoryProfile { get; set; }
        public string SpecialComment { get; set; }
        public string ComplianceConditionCode { get; set; }
        public double CurrentBalance { get; set; }
        public double AmountPastDue { get; set; }
        public double OriginalChargeoffAmount { get; set; }
        public DateTime? DateofAccountInformation { get; set; }
        public DateTime? DateofFirstDelinquency { get; set; } //FCRA Compliance
        public DateTime? DateClosed { get; set; } //closure date
        public DateTime? DateofLastPayment { get; set; } //Last Payment Date
        public string ConsumerTransactionType { get; set; }
        public string SurName { get; set; } //Last Name
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string GenerationCode { get; set; }
        public string SSN { get; set; }
        public string DateofBirth { get; set; }
        public string TelephoneNumber { get; set; }
        public string ECOACode { get; set; }
        public string ConsumerInformationIndicator { get; set; }
        public string CountryCode { get; set; }
        public string FirstLineOfAddress { get; set; }
        public string SecondLineOfAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string AddressIndicator { get; set; }
        public string ResidenceCode { get; set; }
        public DateTime LastReportDate { get; set; }
    }
}
