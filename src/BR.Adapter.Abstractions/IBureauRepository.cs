﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IBureauRepository : IRepository<IBureauMaster>
    {
        Task<List<IBureauMaster>> GetLoansList();
        Task<IBureauMaster> GetLoanInformationByLoanNumber(string loanNumber);
        Task<IBureauMaster> AddReport(string loanNumber, IBaseSegment reportDetail, IJOne j1, IJTwo j2, IKOne k1, IKTwo k2, IKThree k3, IKFour k4, LOne l1, NOne n1);
        Task<IBaseSegment> UpdateBaseSegment(string loanNumber, IBaseSegmentRequest reportDetail);
        Task<IBureauMaster> UpdateFileId(string loanNumber, string FileId, string CommonId);
        Task<IJOne> UpdateJOneSegment(string loanNumber, IJOneRequest J1);
        Task<IJTwo> UpdateJTwoSegment(string loanNumber, IJTwoRequest J2);
        Task<IKOne> UpdateKOneSegment(string loanNumber, IKOneRequest K1);
        Task<IKTwo> UpdateKTwoSegment(string loanNumber, IKTwoRequest K2);
        Task<IKThree> UpdateKThreeSegment(string loanNumber, IKThreeRequest K3);
        Task<IKFour> UpdateKFourSegment(string loanNumber, IKFourRequest K4);
        Task<ILOne> UpdateLOneSegment(string loanNumber, ILOneRequest L1);
        Task<INOne> UpdateNOneSegment(string loanNumber, INOneRequest N1);
        Task<List<string>> GetLoanListByFileId(string fileId);
        Task<IBaseSegment> UpdateMultipleColumn(List<IUpdateRequest> updateRequest, string column, string value);

        Task<List<IBureauMaster>> GetLoanListforFile(string fileId);

    }
}