﻿using LendFoundry.Foundation.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IMultipleUpdateRequest
    {
        [JsonConverter(typeof(InterfaceListConverter<IUpdateRequest, UpdateRequest>))]
        List<IUpdateRequest> UpdateDetails { get; set; }
        string ColumnName { get; set; }
        string ColumnValue { get; set; }
        
    }
}
