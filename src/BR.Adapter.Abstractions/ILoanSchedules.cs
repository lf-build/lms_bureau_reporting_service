﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Services;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface ILoanSchedules : IAggregate
    {
        string LoanNumber { get; set; }
        string ScheduledCreatedReason { get; set; }
        string ModReason { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPaidResponse, PaidResponse>))]
        List<IPaidResponse> Scheduledetails { get; set; }

        //[JsonConverter(typeof(InterfaceConverter<ILoanScheduleDetails, LoanScheduleDetails>))]
        //ILoanScheduleDetails loanDetails { get; set; }

        //[JsonConverter(typeof(InterfaceListConverter<ILoanScheduleTracking, LoanScheduleTracking>))]
        //List<ILoanScheduleTracking> schduleTreckingDetails { get; set; }

    }
}
