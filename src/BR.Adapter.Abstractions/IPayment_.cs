﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace BR.Adapter.Abstractions
{
    public interface IPayment_ : IAggregate
    {
        string LoanNumber { get; set; }

        string ScheduleVersion { get; set; }
        DateTime PaymentDate { get; set; }
        DateTime EffectiveDate { get; set; }
        string Description { get; set; }

        double Principal { get; set; }

        double Interest { get; set; }

        double AdditionalInterest { get; set; }
        double FeeAmount { get; set; }
        double AmountPaid { get; set; }

        double CumulativeFeePaid { get; set; }
        double CumulativePrincipalPaid { get; set; }

        double CumulativeInterestPaid { get; set; }

        double CumulativeAdditionalInterestPaid { get; set; }

        double CumulativeTotalPaid { get; set; }

        DateTime CreatedDate { get; set; }

     
    }
}