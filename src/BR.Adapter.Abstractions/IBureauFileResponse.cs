﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IBureauFileResponse
    {
        string FileId { get; set; }
        TimeBucket FileSentDate { get; set; }
        TimeBucket LastReportedDate { get; set; }
        string FIleName { get; set; }

    }
}
