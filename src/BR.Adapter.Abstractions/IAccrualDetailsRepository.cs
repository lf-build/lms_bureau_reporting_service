﻿using LendFoundry.Foundation.Persistence;
using LMS.LoanAccounting;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IAccrualDetailsRepository : IRepository<IAccrualDetails>
    {
        Task<IAccrualDetails> GetAccrualByLoanNumber(string loanNumber);
        Task<DeleteResult> RemoveHistory();
    }
}
