﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public class JOneRequest : IJOneRequest
    {
        public string Id { get; set; }
        public string FileId { get; set; }
        public string J11 { get; set; }
        public string J12 { get; set; }
        public string J13 { get; set; }
        public DateTime LastReportDate { get; set; }
    }
}
