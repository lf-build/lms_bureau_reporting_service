﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace BR.Adapter.Abstractions
{
    public interface IFileStorageServiceFactory
    {
        IFileStorageService Create(ITokenReader reader, ILogger logger);
    }
}