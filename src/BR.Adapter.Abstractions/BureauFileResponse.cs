﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public class BureauFileResponse : IBureauFileResponse
    {
        public string FileId { get; set; }
        public TimeBucket FileSentDate { get; set; }
        public TimeBucket LastReportedDate { get; set; }
        public string FIleName { get; set; }
    }
}
