﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Services;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public class LoanSchedules : Aggregate, ILoanSchedules
    {
        public string LoanNumber { get; set; }

        public string ScheduledCreatedReason { get; set; }
        public string ModReason { get; set; }
        //[JsonConverter(typeof(InterfaceConverter<ILoanScheduleDetails, LoanScheduleDetails>))]
        //public ILoanScheduleDetails loanDetails { get; set; }

        //[JsonConverter(typeof(InterfaceListConverter<ILoanScheduleTracking, LoanScheduleTracking>))]
        //public List<ILoanScheduleTracking> schduleTreckingDetails { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPaidResponse, PaidResponse>))]
        public List<IPaidResponse> Scheduledetails { get; set; }
    }
}
