﻿using LendFoundry.Security.Identity;
using System;

namespace BR.Adapter.Abstractions
{
    public class AdapterConfiguration
    {

        public bool? IsFilterDate { get; set; }
        public DateTime? ProcessingDate { get; set; }
        public string InnovisProgramIdentifier { get; set; }
        public string EquifaxProgramIdentifier { get; set; }
        public string ExperianProgramIdentifier { get; set; }
        public string TransUnionProgramIdentifier { get; set; }
        public string ReporterName { get; set; }
        public string ReporterAddress { get; set; }
        public string ReporterTelephoneNumber { get; set; }
        public string SoftwareVendorName { get; set; }
        public string SoftwareVersionNumber { get; set; }
        public string AccountType { get; set; }
        public string ECOACode { get; set; }
        public string CountryCode { get; set; }
        public string InterestRateIndicator { get; set; }
        public string IdentificationNumber { get; set; }
        public string ProcessingIndicator { get; set; }
        public string VersionReason { get; set; }
        public string SubReason { get; set; }


        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string SshHostKey { get; set; }
        public string UploadPath { get; set; }

        // Correction Indicator

        public string PaidOffCode { get; set; }
        public string ChargedOffCode { get; set; }
        public string SoldOffCode { get; set; }
        public string ClosedCode { get; set; }

    }
}
