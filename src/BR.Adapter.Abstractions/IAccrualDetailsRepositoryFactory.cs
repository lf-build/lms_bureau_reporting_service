﻿using LendFoundry.Security.Tokens;

namespace BR.Adapter.Abstractions
{
    public interface IAccrualDetailsRepositoryFactory
    {
        IAccrualDetailsRepository Create(ITokenReader reader);
    }
}