﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IBureauService
    {
        Task<string> ProcessLoans();
        Task<string> CreateFile(string fileId);
        Task<List<IBureauFileResponse>> GetBureauSummaryByLoannumber(string loannumber);
        Task<IBaseSegment> GetBaseSegment(string loanNumber, string fileId);
        Task<IJOne> GetJOneSegment(string loanNumber, string fileId);
        Task<IJTwo> GetJTwoSegment(string loanNumber, string fileId);
        Task<IKOne> GetKOneSegment(string loanNumber, string fileId);
        Task<IKTwo> GetKTwoSegment(string loanNumber, string fileId);
        Task<IKThree> GetKThreeSegment(string loanNumber, string fileId);
        Task<IKFour> GetKFourSegment(string loanNumber, string fileId);
        Task<ILOne> GetLOneSegment(string loanNumber, string fileId);
        Task<INOne> GetNOneSegment(string loanNumber, string fileId);
        Task<IBureauMaster> GetAllSegment(string loanNumber, string fileId);
        Task<List<IBureauFile>> GetAllFiles();
        Task<List<string>> GetFileDetailsByFileId(string fileId);
        Task<IBaseSegment> UpdateBaseSegment(string loanNumber, BaseSegmentRequest basesegment);
        Task<IJOne> UpdateJOneSegment(string loanNumber, JOneRequest J1);
        Task<IJTwo> UpdateJTwoSegment(string loanNumber, JTwoRequest J2);
        Task<IKOne> UpdateKOneSegment(string loanNumber, KOneRequest K1);
        Task<IKTwo> UpdateKTwoSegment(string loanNumber, KTwoRequest K2);
        Task<IKThree> UpdateKThreeSegment(string loanNumber, KThreeRequest K3);
        Task<IKFour> UpdateKFourSegment(string loanNumber, KFourRequest K4);
        Task<ILOne> UpdateLOneSegment(string loanNumber, LOneRequest L1);
        Task<INOne> UpdateNOneSegment(string loanNumber, NOneRequest N1);
        Task<IBaseSegment> UpdateMultipleLoans(MultipleUpdateRequest requestdata);

    }
}
