using System;

namespace BR.Adapter.Abstractions
{
    public interface IRecoverabilityPolicy
    {
        bool CanBeRecoveredFrom(Exception exception);
    }
}