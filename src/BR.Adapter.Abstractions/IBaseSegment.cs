﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IBaseSegment
    {
        string Id { get; set; }
        string FileId { get; set; }
        string ConsumerAccountNumber { get; set; }
        string PortfolioType { get; set; }
        string AccountType { get; set; }
        string CorrectionIndicator { get; set; }
        string CycleIdentifier { get; set; }
        string InterestTypeIndicator { get; set; }
        TimeBucket DateOpened { get; set; }
        double CreditLimit { get; set; }
        double HighestCredit { get; set; }
        int TermsDuration { get; set; }
        string TermsFrequency { get; set; }
        double ScheduledMonthlyPaymentAmount { get; set; }
        double ActualPaymentAmount { get; set; }
        string AccountStatus { get; set; }
        string PaymentRating { get; set; }
        string PaymentHistoryProfile { get; set; }
        string SpecialComment { get; set; }
        string ComplianceConditionCode { get; set; }
        double CurrentBalance { get; set; }
        double AmountPastDue { get; set; }
        double OriginalChargeoffAmount { get; set; }
        TimeBucket DateofAccountInformation { get; set; }
        TimeBucket DateofFirstDelinquency { get; set; } //FCRA Compliance
        TimeBucket DateClosed { get; set; } //closure date
        TimeBucket DateofLastPayment { get; set; } //Last Payment Date
        string ConsumerTransactionType { get; set; }
        string SurName { get; set; } //Last Name
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string GenerationCode { get; set; }
        string SSN { get; set; }
        string DateofBirth { get; set; }
        string TelephoneNumber { get; set; }
        string ECOACode { get; set; }
        string ConsumerInformationIndicator { get; set; }
        string CountryCode { get; set; }
        string FirstLineOfAddress { get; set; }
        string SecondLineOfAddress { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string AddressIndicator { get; set; }
        string ResidenceCode { get; set; }
        TimeBucket LastReportDate { get; set; }
    }
}
