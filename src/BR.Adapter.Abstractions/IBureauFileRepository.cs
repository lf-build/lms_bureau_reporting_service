﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IBureauFileRepository : IRepository<IBureauFile>
    {
        Task<List<IBureauFile>> GetFileList();
        Task<IBureauFile> GetFileByFileId(string loanNumber);

    }
}