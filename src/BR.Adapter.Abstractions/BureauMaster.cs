﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public class BureauMaster : Aggregate, IBureauMaster
    {

        public string LoanNumber { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBaseSegment, BaseSegment>))]
        public List<IBaseSegment> BaseSegments { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IJOne, JOne>))]
        public List<IJOne> J1s { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IJTwo, JTwo>))]
        public List<IJTwo> J2s { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IKOne, KOne>))]
        public List<IKOne> K1 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IKTwo, KTwo>))]
        public List<IKTwo> K2 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IKThree, KThree>))]
        public List<IKThree> K3 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IKFour, KFour>))]
        public List<IKFour> K4 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ILOne, LOne>))]
        public List<ILOne> L1 { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<INOne, NOne>))]
        public List<INOne> N1 { get; set; }
        public TimeBucket BureauCreateDate { get; set; } //DateBougthInBureau - first time loan added to bureau

    }
}
