﻿using LendFoundry.Security.Tokens;

namespace BR.Adapter.Abstractions
{
    public interface IAdapterRepositoryFactory
    {
        IAdapterRepository Create(ITokenReader reader);
    }
}