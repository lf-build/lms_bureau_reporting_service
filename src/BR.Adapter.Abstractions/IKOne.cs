﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IKOne
    {
        string FileId { get; set; }
        string Id { get; set; }
        string K1 { get; set; }
        TimeBucket LastReportDate { get; set; }
    }
}
