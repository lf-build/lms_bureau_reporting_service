﻿using LendFoundry.Foundation.Persistence;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IScheduleRepository : IRepository<ILoanSchedules>
    {
        Task<ILoanSchedules> GetScheduleByLoanNumber(string loanNumber);

        Task<DeleteResult> RemoveHistory();
    }
}
