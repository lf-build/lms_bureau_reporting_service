﻿using System.Collections.Generic;
using System.IO;

namespace BR.Adapter
{
    public interface IFileStorageService
    {
        //    void Upload(string fileFullName, string destinationPath);
        //void Upload(Stream stream, string fileName, string destinationPath);
        void Upload(byte[] stream, string fileName, string destinationPath);
        //List<string> DownloadFiles(string sourcePath, string destinationPath);

        //bool IsExist(string fileName, string outboxLocation);
        //void Delete(string fileName, string outboxLocation);
    }
}
