﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public class BureauFile : Aggregate, IBureauFile
    {
        public TimeBucket FileSentDate { get; set; }
        public string FileName { get; set; }
        public string FileId { get; set; }
        public TimeBucket CreatedDate { get; set; }
    }
}
