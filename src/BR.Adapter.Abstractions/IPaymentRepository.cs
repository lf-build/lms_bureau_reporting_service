﻿using LendFoundry.Foundation.Persistence;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IPaymentRepository : IRepository<LMS.LoanAccounting.IPayment>
    {
        Task<List<LMS.LoanAccounting.IPayment>> GetPayments(string loanNumber);

        Task<DeleteResult> RemoveHistory();
    }
}
