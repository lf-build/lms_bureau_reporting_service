﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IUpdateRequest
    {
        string LoanNumber { get; set; }
        string ReportId { get; set; }
    }
}
