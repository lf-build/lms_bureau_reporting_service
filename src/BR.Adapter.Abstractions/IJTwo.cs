﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LMS.Loan.Filters.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BR.Adapter.Abstractions
{
    public interface IJTwo
    {
        string FileId { get; set; }
        string Id { get; set; }
        string J21 { get; set; }
        string J22 { get; set; }
        string J23 { get; set; }
        TimeBucket LastReportDate { get; set; }
    }
}
