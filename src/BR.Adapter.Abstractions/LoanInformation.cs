﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;
using LendFoundry.Foundation.Services;

namespace BR.Adapter.Abstractions
{
    public class LoanInformation : LMS.Loan.Filters.FilterView, ILoanInformation
    {

        public List<LMS.LoanManagement.Abstractions.IPaymentInstrument> PaymentInstruments { get; set; }
        [JsonConverter(typeof(InterfaceConverter<LMS.LoanManagement.Abstractions.IApplicantDetails, LMS.LoanManagement.Abstractions.ApplicantDetails>))]
        public List<LMS.LoanManagement.Abstractions.IApplicantDetails> ApplicantDetails { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<LMS.LoanManagement.Abstractions.IBusinessDetails, LMS.LoanManagement.Abstractions.BusinessDetails>))]
        public List<LMS.LoanManagement.Abstractions.IBusinessDetails> BusinessDetails { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<LMS.LoanManagement.Abstractions.IDrawDownDetails, LMS.LoanManagement.Abstractions.DrawDownDetails>))]
        public List<LMS.LoanManagement.Abstractions.IDrawDownDetails> DrawDownDetails { get; set; }
        public LMS.LoanManagement.Abstractions.Currency Currency { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<LMS.LoanManagement.Abstractions.IOtherContact, LMS.LoanManagement.Abstractions.OtherContact>))]
        public List<LMS.LoanManagement.Abstractions.IOtherContact> OtherContacts { get; set; }

    }
}
