﻿using LendFoundry.Security.Tokens;

namespace BR.Adapter.Abstractions
{
    public interface IBureauRepositoryFactory
    {
        IBureauRepository Create(ITokenReader reader);
    }
}