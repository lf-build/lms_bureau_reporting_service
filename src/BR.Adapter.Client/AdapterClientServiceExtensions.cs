﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace BR.Adapter.Client
{
    public static class AdapterClientServiceExtensions
    {
        public static IServiceCollection AddAdapterService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IAdapterClientServiceFactory>(p => new AdapterClientServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IAdapterClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
