﻿using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using RestSharp;
using System.Linq;
using BR.Adapter.Abstractions;

namespace BR.Adapter.Client
{
    public class BureauClientService : IBureauService
    {
        public BureauClientService(IServiceClient client) { Client = client; }

        private IServiceClient Client { get; }

        public async Task<string> ProcessLoans()
        {
            var request = new RestRequest("/process/loan", Method.GET);
            return await Client.ExecuteAsync<string>(request);
        }

       public Task<List<IBureauFile>> GetAllFiles()
        {
            throw new NotImplementedException();
        }

        public Task<IBureauMaster> GetAllSegment(string loanNumber, string fileId)
        {
            throw new NotImplementedException();
        }

        public Task<IBaseSegment> GetBaseSegment(string loanNumber, string fileId)
        {
            throw new NotImplementedException();
        }

        public Task<List<IBureauFileResponse>> GetBureauSummaryByLoannumber(string loannumber)
        {
            throw new NotImplementedException();
        }

        public Task<List<string>> GetFileDetailsByFileId(string fileId)
        {
            throw new NotImplementedException();
        }

        public Task<IJOne> GetJOneSegment(string loanNumber, string fileId)
        {
            throw new NotImplementedException();
        }

        public Task<IJTwo> GetJTwoSegment(string loanNumber, string fileId)
        {
            throw new NotImplementedException();
        }

        public Task<IKFour> GetKFourSegment(string loanNumber, string fileId)
        {
            throw new NotImplementedException();
        }

        public Task<IKOne> GetKOneSegment(string loanNumber, string fileId)
        {
            throw new NotImplementedException();
        }

        public Task<IKThree> GetKThreeSegment(string loanNumber, string fileId)
        {
            throw new NotImplementedException();
        }

        public Task<IKTwo> GetKTwoSegment(string loanNumber, string fileId)
        {
            throw new NotImplementedException();
        }

        public Task<ILOne> GetLOneSegment(string loanNumber, string fileId)
        {
            throw new NotImplementedException();
        }

        public Task<INOne> GetNOneSegment(string loanNumber, string fileId)
        {
            throw new NotImplementedException();
        }

      

        public Task<IBaseSegment> UpdateBaseSegment(string loanNumber, BaseSegmentRequest basesegment)
        {
            throw new NotImplementedException();
        }

        public Task<IJOne> UpdateJOneSegment(string loanNumber, JOneRequest J1)
        {
            throw new NotImplementedException();
        }

        public Task<IJTwo> UpdateJTwoSegment(string loanNumber, JTwoRequest J2)
        {
            throw new NotImplementedException();
        }

        public Task<IKFour> UpdateKFourSegment(string loanNumber, KFourRequest K4)
        {
            throw new NotImplementedException();
        }

        public Task<IKOne> UpdateKOneSegment(string loanNumber, KOneRequest K1)
        {
            throw new NotImplementedException();
        }

        public Task<IKThree> UpdateKThreeSegment(string loanNumber, KThreeRequest K3)
        {
            throw new NotImplementedException();
        }

        public Task<IKTwo> UpdateKTwoSegment(string loanNumber, KTwoRequest K2)
        {
            throw new NotImplementedException();
        }

        public Task<ILOne> UpdateLOneSegment(string loanNumber, LOneRequest L1)
        {
            throw new NotImplementedException();
        }

        public Task<IBaseSegment> UpdateMultipleLoans(MultipleUpdateRequest requestdata)
        {
            throw new NotImplementedException();
        }

        public Task<INOne> UpdateNOneSegment(string loanNumber, NOneRequest N1)
        {
            throw new NotImplementedException();
        }

        public Task<string> CreateFile(string fileId)
        {
            throw new NotImplementedException();
        }
    }
}
