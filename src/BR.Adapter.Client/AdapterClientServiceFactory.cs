﻿using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using BR.Adapter.Abstractions;
using System;

namespace BR.Adapter.Client
{
    public class AdapterClientServiceFactory : IAdapterClientServiceFactory
    {
        public AdapterClientServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IAdapterService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new AdapterClientService(client);
        }
       
    }
}
