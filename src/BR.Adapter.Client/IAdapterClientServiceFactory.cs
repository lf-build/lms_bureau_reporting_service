﻿using LendFoundry.Security.Tokens;
using BR.Adapter.Abstractions;

namespace BR.Adapter.Client
{
    public interface IAdapterClientServiceFactory
    {
        IAdapterService Create(ITokenReader reader);
    }
}
