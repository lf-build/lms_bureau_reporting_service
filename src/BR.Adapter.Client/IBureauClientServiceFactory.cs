﻿using LendFoundry.Security.Tokens;
using BR.Adapter.Abstractions;

namespace BR.Adapter.Client
{
    public interface IBureauClientServiceFactory
    {
        IBureauService Create(ITokenReader reader);
    }
}
