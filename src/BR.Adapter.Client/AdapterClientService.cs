﻿using System;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using System.Collections.Generic;
using RestSharp;
using System.Linq;
using BR.Adapter.Abstractions;

namespace BR.Adapter.Client
{
    public class AdapterClientService : IAdapterService
    {
        public AdapterClientService(IServiceClient client) { Client = client; }

        private IServiceClient Client { get; }

        public async Task<string> ExctractLoanInformation()
        {
            var request = new RestRequest("/exctract/loan", Method.GET);
            return await Client.ExecuteAsync<string>(request);
        }

    }
}
