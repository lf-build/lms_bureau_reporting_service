﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace BR.Adapter.Client
{
    public static class BureauClientServiceExtensions
    {
        public static IServiceCollection AddBureauService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IBureauClientServiceFactory>(p => new BureauClientServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IBureauClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
