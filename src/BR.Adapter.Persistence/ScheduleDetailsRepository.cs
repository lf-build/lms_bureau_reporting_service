﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using BR.Adapter.Abstractions;

namespace BR.Adapter.Persistence
{
    public class ScheduleDetailsRepository : MongoRepository<ILoanSchedules, LoanSchedules>, IScheduleRepository
    {
        static ScheduleDetailsRepository()
        {

            BsonClassMap.RegisterClassMap<LoanSchedules>(map =>
            {
                map.AutoMap();
                var type = typeof(LoanSchedules);
                //map.MapMember(m => m.PaymentDate).SetSerializer(new DateTimeSerializer(true));
                //map.MapMember(m => m.EffectiveDate).SetSerializer(new DateTimeSerializer(true));
                //map.MapMember(m => m.ScheduleDate).SetSerializer(new DateTimeSerializer(true));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<LMS.LoanAccounting.PaidResponse>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanAccounting.PaidResponse);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.LoanScheduleDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.LoanScheduleDetails);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);

            });

            BsonClassMap.RegisterClassMap<LMS.LoanAccounting.LoanScheduleTracking>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanAccounting.LoanScheduleTracking);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");

            });

        }

        public ScheduleDetailsRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "schedule-details")
        {


        }

        public async Task<Abstractions.ILoanSchedules> GetScheduleByLoanNumber(string loanNumber)
        {
            return await Query.FirstOrDefaultAsync(i => i.LoanNumber == loanNumber);
        }

        public async Task<DeleteResult> RemoveHistory()
        {
            return await Collection.DeleteManyAsync(Builders<ILoanSchedules>.Filter.Where(a => a.TenantId == TenantService.Current.Id));
        }

    }
}