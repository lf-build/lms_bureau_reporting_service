﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using LMS.LoanAccounting;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using BR.Adapter.Abstractions;

namespace BR.Adapter.Persistence
{
    public class AccrualDetailsRepository : MongoRepository<Abstractions.IAccrualDetails, Abstractions.AccrualDetails>, Abstractions.IAccrualDetailsRepository
    {
        static AccrualDetailsRepository()
        {

            BsonClassMap.RegisterClassMap<Accrual>(map =>
            {
                map.AutoMap();
                var type = typeof(Accrual);
                map.MapMember(m => m.StartDate).SetSerializer(new DateTimeSerializer(true));
                map.MapMember(m => m.AccrualStopDate).SetSerializer(new DateTimeSerializer(true));
                map.MapMember(m => m.ProcessingDate).SetSerializer(new DateTimeSerializer(true));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            //BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            //{
            //    map.AutoMap();
            //    map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

            //    var type = typeof(TimeBucket);
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(false);
            //});


            BsonClassMap.RegisterClassMap<PBOTAccrual>(map =>
            {
                map.AutoMap();

                var type = typeof(PBOTAccrual);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
            //BsonClassMap.RegisterClassMap<PBOC>(map =>
            //{
            //    map.AutoMap();

            //    var type = typeof(PBOC);
            //    map.MapMember(m => m.AsOfDatePBOC).SetSerializer(new DateTimeSerializer(true));
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(false);
            //});

            //BsonClassMap.RegisterClassMap<PayOff>(map =>
            //{
            //    map.AutoMap();

            //    var type = typeof(PayOff);
            //    map.MapMember(m => m.PayOffDate).SetSerializer(new DateTimeSerializer(true));
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(false);
            //});
            BsonClassMap.RegisterClassMap<Schedule>(map =>
            {
                map.AutoMap();
                var type = typeof(Schedule);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
            //BsonClassMap.RegisterClassMap<SellOff>(map =>
            //{
            //    map.AutoMap();

            //    var type = typeof(PayOff);
            //    map.MapMember(m => m.SellOffDate).SetSerializer(new DateTimeSerializer(true));
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(false);
            //});
            //BsonClassMap.RegisterClassMap<ChargeOff>(map =>
            //{
            //    map.AutoMap();

            //    var type = typeof(ChargeOff);
            //    map.MapMember(m => m.ChargeOffDate).SetSerializer(new DateTimeSerializer(true));
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(false);
            //});
            //BsonClassMap.RegisterClassMap<PBOT>(map =>
            //{
            //    map.AutoMap();

            //    var type = typeof(PBOT);
            //    map.MapMember(m => m.AsOfDatePBOT).SetSerializer(new DateTimeSerializer(true));
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(false);
            //});
            //BsonClassMap.RegisterClassMap<Due>(map =>
            //{
            //    map.AutoMap();

            //    var type = typeof(Due);
            //    map.MapMember(m => m.DPDStartDate).SetSerializer(new DateTimeSerializer(true));
            //    map.MapMember(m => m.PaymentMissedDate).SetSerializer(new DateTimeSerializer(true));
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(false);
            //});

            BsonClassMap.RegisterClassMap<PBOCAccrual>(map =>
            {
                map.AutoMap();

                var type = typeof(PBOCAccrual);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            //BsonClassMap.RegisterClassMap<PaymentInfo>(map =>
            //{
            //    map.AutoMap();

            //    var type = typeof(PaymentInfo);
            //    map.MapMember(m => m.LastPaymentReceivedDate).SetSerializer(new DateTimeSerializer(true));
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(false);
            //});

            //BsonClassMap.RegisterClassMap<ExcessMoney>(map =>
            //{
            //    map.AutoMap();

            //    var type = typeof(ExcessMoney);
            //    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            //    map.SetIsRootClass(false);
            //});
        }

        public AccrualDetailsRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "accrual-balances")
        {


        }

        public async Task<Abstractions.IAccrualDetails> GetAccrualByLoanNumber(string loanNumber)
        {
            return await Query.FirstOrDefaultAsync(i => i.LoanNumber == loanNumber);
        }

        public async Task<DeleteResult> RemoveHistory()
        {
            return await Collection.DeleteManyAsync(Builders<IAccrualDetails>.Filter.Where(a => a.TenantId == TenantService.Current.Id));
        }

    }
}