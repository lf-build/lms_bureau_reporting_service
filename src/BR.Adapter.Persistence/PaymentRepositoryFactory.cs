﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using BR.Adapter.Abstractions;
using LendFoundry.Security.Encryption;

namespace BR.Adapter.Persistence
{
    public class PaymentRepositoryFactory : IPaymentRepositoryFactory
    {
        public PaymentRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IPaymentRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            
            return new PaymentRepository(tenantService, mongoConfiguration);
        }
    }
}
