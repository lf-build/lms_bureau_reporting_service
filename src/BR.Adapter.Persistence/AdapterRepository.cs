﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver.Linq;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using System;
using MongoDB.Driver;
using BR.Adapter.Abstractions;
using LendFoundry.Security.Encryption;
using CreditExchange.StatusManagement;
using LMS.LoanAccounting;
using LMS.Loan.Filters.Abstractions;
using System.Collections.Generic;

namespace BR.Adapter.Persistence
{
    public class AdapterRepository : MongoRepository<ILoanInformation, LoanInformation>, IAdapterRepository
    {
        static AdapterRepository()
        {


            BsonClassMap.RegisterClassMap<LoanInformation>(map =>
            {
                map.AutoMap();
                var type = typeof(LoanInformation);
                //map.MapProperty(p => p.ProductPortfolioType).SetSerializer(new EnumSerializer<LMS.LoanManagement.Abstractions.ProductPortfolioType>(BsonType.String));
                map.MapProperty(p => p.Currency).SetSerializer(new EnumSerializer<LMS.LoanManagement.Abstractions.Currency>(BsonType.String));

                //map.MapProperty(p => p.ProductCategory).SetSerializer(new EnumSerializer<LMS.LoanManagement.Abstractions.ProductCategory>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.ApplicantDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.ApplicantDetails);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.DrawDownDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.DrawDownDetails);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.Email>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.Email);
                map.MapProperty(p => p.EmailType).SetSerializer(new EnumSerializer<LMS.LoanManagement.Abstractions.EmailType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.Address>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.Address);
                map.MapProperty(p => p.AddressType).SetSerializer(new EnumSerializer<LMS.LoanManagement.Abstractions.AddressType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.Phone>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.Phone);
                map.MapProperty(p => p.PhoneType).SetSerializer(new EnumSerializer<LMS.LoanManagement.Abstractions.PhoneType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.Name>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.Name);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.PaymentInstrument>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.PaymentInstrument);
                map.MapProperty(p => p.PaymentInstrumentType).SetSerializer(new EnumSerializer<LMS.LoanManagement.Abstractions.PaymentInstrumentType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.OtherContact>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.OtherContact);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.OtherEmail>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.OtherEmail);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.OtherPhone>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.OtherPhone);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.OtherAddress>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanManagement.Abstractions.OtherAddress);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

          
            BsonClassMap.RegisterClassMap<SubStatus>(map =>
            {
                map.AutoMap();
                var type = typeof(SubStatus);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<PBOC>(map =>
            {
                map.AutoMap();
                var type = typeof(PBOC);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            BsonClassMap.RegisterClassMap<PBOT>(map =>
            {
                map.AutoMap();
                var type = typeof(PBOT);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
            BsonClassMap.RegisterClassMap<ExcessMoney>(map =>
            {
                map.AutoMap();
                var type = typeof(ExcessMoney);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            BsonClassMap.RegisterClassMap<PaymentInfo>(map =>
            {
                map.AutoMap();
                var type = typeof(PaymentInfo);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            BsonClassMap.RegisterClassMap<ChargeOff>(map =>
            {
                map.AutoMap();
                var type = typeof(ChargeOff);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            BsonClassMap.RegisterClassMap<Due>(map =>
            {
                map.AutoMap();
                var type = typeof(Due);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            BsonClassMap.RegisterClassMap<PayOff>(map =>
            {
                map.AutoMap();
                var type = typeof(PayOff);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            BsonClassMap.RegisterClassMap<SellOff>(map =>
            {
                map.AutoMap();
                var type = typeof(SellOff);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            BsonClassMap.RegisterClassMap<PaymentDetails>(map =>
            {
                map.AutoMap();
                var type = typeof(PaymentDetails);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });

            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
        }
        public AdapterRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService)
            : base(tenantService, configuration, "loan-information-extract")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(LMS.LoanManagement.Abstractions.PII)))
            {
                BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.PII>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.DOB).SetSerializer(new BsonEncryptor<DateTime, DateTime>(encrypterService));
                    map.MapMember(m => m.SSN).SetSerializer(new BsonEncryptor<string, string>(encrypterService));
                    var type = typeof(LMS.LoanManagement.Abstractions.PII);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                    map.SetIsRootClass(true);
                });
            }
            if (!BsonClassMap.IsClassMapRegistered(typeof(LMS.LoanManagement.Abstractions.BusinessDetails)))
            {
                BsonClassMap.RegisterClassMap<LMS.LoanManagement.Abstractions.BusinessDetails>(map =>
                {
                    map.AutoMap();
                    map.MapMember(m => m.FedTaxId).SetSerializer(new BsonEncryptor<string, string>(encrypterService));
                    var type = typeof(LMS.LoanManagement.Abstractions.BusinessDetails);
                    map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                });
            }
        }
        public async Task<List<ILoanInformation>> GetLoansList()
        {
            return await Query.Where(loanInformation => loanInformation.TenantId == TenantService.Current.Id).ToListAsync();
        }
               
        public async Task<DeleteResult> RemoveHistory()
        {
            return await Collection.DeleteManyAsync(Builders<ILoanInformation>.Filter.Where(a => a.TenantId == TenantService.Current.Id));
        }

        //public async Task<List<IAccrual>> GetHistory(string loanNumber, string scheduleVersion)
        //{
        //    return await Query.Where(i => i.LoanNumber == loanNumber && i.ScheduleVersion == scheduleVersion).OrderByDescending(j => j.ProcessingDate).ToListAsync();
        //}

    }
}