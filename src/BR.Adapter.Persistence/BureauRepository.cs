﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver.Linq;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using System;
using MongoDB.Driver;
using BR.Adapter.Abstractions;
using LendFoundry.Security.Encryption;
using CreditExchange.StatusManagement;
using LMS.LoanAccounting;
using LMS.Loan.Filters.Abstractions;
using System.Collections.Generic;

namespace BR.Adapter.Persistence
{
    public class BureauRepository : MongoRepository<IBureauMaster, BureauMaster>, IBureauRepository
    {
        static BureauRepository()
        {
            BsonClassMap.RegisterClassMap<BureauMaster>(map =>
            {
                map.AutoMap();
                var type = typeof(BureauMaster);
                //map.MapProperty(p => p.ProductPortfolioType).SetSerializer(new EnumSerializer<LMS.LoanManagement.Abstractions.ProductPortfolioType>(BsonType.String));
                // map.MapProperty(p => p.Currency).SetSerializer(new EnumSerializer<LMS.LoanManagement.Abstractions.Currency>(BsonType.String));

                //map.MapProperty(p => p.ProductCategory).SetSerializer(new EnumSerializer<LMS.LoanManagement.Abstractions.ProductCategory>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<BaseSegment>(map =>
            {
                map.AutoMap();
                var type = typeof(BaseSegment);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);

            });

            BsonClassMap.RegisterClassMap<JOne>(map =>
            {
                map.AutoMap();
                var type = typeof(JOne);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);

            });

            BsonClassMap.RegisterClassMap<JTwo>(map =>
            {
                map.AutoMap();
                var type = typeof(JTwo);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);

            });

            BsonClassMap.RegisterClassMap<KOne>(map =>
            {
                map.AutoMap();
                var type = typeof(KOne);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<KTwo>(map =>
            {
                map.AutoMap();
                var type = typeof(KTwo);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<KThree>(map =>
            {
                map.AutoMap();
                var type = typeof(KThree);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<KFour>(map =>
            {
                map.AutoMap();
                var type = typeof(KFour);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<LOne>(map =>
            {
                map.AutoMap();
                var type = typeof(LOne);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<NOne>(map =>
            {
                map.AutoMap();
                var type = typeof(NOne);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

        }
        public BureauRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService)
            : base(tenantService, configuration, "BureauMaster")
        {

        }
        public async Task<List<IBureauMaster>> GetLoansList()
        {
            return await Query.Where(loanInformation => loanInformation.TenantId == TenantService.Current.Id).ToListAsync();
        }
        public async Task<IBureauMaster> GetLoanInformationByLoanNumber(string loanNumber)
        {
            return await Query.FirstOrDefaultAsync(loanInformation => loanInformation.TenantId == TenantService.Current.Id && loanInformation.LoanNumber == loanNumber);
        }

        public async Task<IBureauMaster> AddReport(string loanApplicationNumber, IBaseSegment reportDetail, IJOne j1, IJTwo j2, IKOne k1, IKTwo k2, IKThree k3, IKFour k4, LOne l1, NOne n1)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanApplicationNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Loan Information for {loanApplicationNumber} not found");

            loanInformation.BaseSegments.Add(reportDetail);
            loanInformation.J1s.Add(j1);
            loanInformation.J2s.Add(j2);
            loanInformation.K1.Add(k1);
            loanInformation.K2.Add(k2);
            loanInformation.K3.Add(k3);
            loanInformation.K4.Add(k4);
            loanInformation.L1.Add(l1);
            loanInformation.N1.Add(n1);

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                               a.LoanNumber == loanInformation.LoanNumber),
            Builders<IBureauMaster>.Update.Set(a => a.BaseSegments, loanInformation.BaseSegments)
            .Set(a => a.J1s, loanInformation.J1s)
            .Set(a => a.J2s, loanInformation.J2s)
            .Set(a => a.K1, loanInformation.K1)
            .Set(a => a.K2, loanInformation.K2)
            .Set(a => a.K3, loanInformation.K3)
            .Set(a => a.K4, loanInformation.K4)
            .Set(a => a.L1, loanInformation.L1)
            .Set(a => a.N1, loanInformation.N1));

            return loanInformation;
        }

        public async Task<IBureauMaster> UpdateFileId(string loanNumber, string FileId, string commonId)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Report Information for {loanNumber} not found");

            IBaseSegment basesegment = null;
            IJOne jone = null;
            IJTwo jtwo = null;
            IKOne kone = null;
            IKTwo ktwo = null;
            IKThree kthree = null;
            IKFour Kfour = null;
            ILOne lone = null;
            INOne none = null;

            basesegment = loanInformation.BaseSegments.Where(a => a.Id == commonId).FirstOrDefault();
            jone = loanInformation.J1s.Where(a => a.Id == commonId).FirstOrDefault();
            jtwo = loanInformation.J2s.Where(a => a.Id == commonId).FirstOrDefault();
            kone = loanInformation.K1.Where(a => a.Id == commonId).FirstOrDefault();
            ktwo = loanInformation.K2.Where(a => a.Id == commonId).FirstOrDefault();
            kthree = loanInformation.K3.Where(a => a.Id == commonId).FirstOrDefault();
            Kfour = loanInformation.K4.Where(a => a.Id == commonId).FirstOrDefault();
            lone = loanInformation.L1.Where(a => a.Id == commonId).FirstOrDefault();
            none = loanInformation.N1.Where(a => a.Id == commonId).FirstOrDefault();

            if (basesegment == null)
                throw new NotFoundException($"basesegment for {basesegment.Id} not found");
            if (jone == null)
                throw new NotFoundException($"J1 for {jone.Id} not found");
            if (jtwo == null)
                throw new NotFoundException($"J2 for {jtwo.Id} not found");
            if (kone == null)
                throw new NotFoundException($"K1 for {kone.Id} not found");
            if (ktwo == null)
                throw new NotFoundException($"K2 for {ktwo.Id} not found");
            if (kthree == null)
                throw new NotFoundException($"K3 for {kthree.Id} not found");
            if (Kfour == null)
                throw new NotFoundException($"K4 for {Kfour.Id} not found");
            if (lone == null)
                throw new NotFoundException($"L1 for {lone.Id} not found");
            if (none == null)
                throw new NotFoundException($"N1 for {none.Id} not found");

            basesegment.FileId = FileId;
            jone.FileId = FileId;
            jtwo.FileId = FileId;
            kone.FileId = FileId;
            ktwo.FileId = FileId;
            kthree.FileId = FileId;
            Kfour.FileId = FileId;
            lone.FileId = FileId;
            none.FileId = FileId;

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.BaseSegments, loanInformation.BaseSegments));

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                       a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.J1s, loanInformation.J1s));

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                 a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.J2s, loanInformation.J2s));
            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                 a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.K1, loanInformation.K1));
            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                 a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.K2, loanInformation.K2));
            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                 a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.K3, loanInformation.K3));
            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                 a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.K4, loanInformation.K4));
            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                 a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.L1, loanInformation.L1));
            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                a.LoanNumber == loanNumber),
               Builders<IBureauMaster>.Update.Set(a => a.N1, loanInformation.N1));

            return loanInformation;
        }
        public async Task<IBaseSegment> UpdateBaseSegment(string loanNumber, IBaseSegmentRequest baseSegmentDetail)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Report Information for {loanNumber} not found");

            IBaseSegment basesegment = null;

            basesegment = loanInformation.BaseSegments.Where(a => a.Id == baseSegmentDetail.Id).FirstOrDefault();

            if (basesegment == null)
                throw new NotFoundException($"basesegment for {baseSegmentDetail.Id} not found");

            basesegment.AccountStatus = baseSegmentDetail.AccountStatus;
            basesegment.AccountType = baseSegmentDetail.AccountType;
            basesegment.ActualPaymentAmount = baseSegmentDetail.ActualPaymentAmount;
            basesegment.AddressIndicator = baseSegmentDetail.AddressIndicator;
            basesegment.AmountPastDue = baseSegmentDetail.AmountPastDue;
            basesegment.City = baseSegmentDetail.City;
            basesegment.ComplianceConditionCode = baseSegmentDetail.ComplianceConditionCode;
            basesegment.ConsumerAccountNumber = baseSegmentDetail.ConsumerAccountNumber;
            basesegment.ConsumerInformationIndicator = baseSegmentDetail.ConsumerInformationIndicator;
            basesegment.ConsumerTransactionType = baseSegmentDetail.ConsumerTransactionType;
            basesegment.CorrectionIndicator = baseSegmentDetail.CorrectionIndicator;
            basesegment.CycleIdentifier = baseSegmentDetail.CycleIdentifier;
            basesegment.CountryCode = baseSegmentDetail.CountryCode;
            basesegment.CreditLimit = baseSegmentDetail.CreditLimit;
            basesegment.CurrentBalance = baseSegmentDetail.CurrentBalance;
            if (baseSegmentDetail.DateClosed != null)
                basesegment.DateClosed = new TimeBucket(baseSegmentDetail.DateClosed.Value);
            if (baseSegmentDetail.DateofAccountInformation != null)
                basesegment.DateofAccountInformation = new TimeBucket(baseSegmentDetail.DateofAccountInformation.Value);
            if (baseSegmentDetail.DateofFirstDelinquency != null)
                basesegment.DateofFirstDelinquency = new TimeBucket(baseSegmentDetail.DateofFirstDelinquency.Value);
            if (baseSegmentDetail.DateofLastPayment != null)
                basesegment.DateofLastPayment = new TimeBucket(baseSegmentDetail.DateofLastPayment.Value);


            // basesegment.DateClosed = new TimeBucket(baseSegmentDetail.DateClosed);
            //basesegment.DateofAccountInformation = new TimeBucket(baseSegmentDetail.DateofAccountInformation);
            //basesegment.DateofFirstDelinquency = new TimeBucket(baseSegmentDetail.DateofFirstDelinquency);
            //basesegment.DateofLastPayment = new TimeBucket(baseSegmentDetail.DateofLastPayment);
            basesegment.DateofBirth = baseSegmentDetail.DateofBirth;

            basesegment.DateOpened = new TimeBucket(baseSegmentDetail.DateOpened);
            basesegment.ECOACode = baseSegmentDetail.ECOACode;
            basesegment.FirstLineOfAddress = baseSegmentDetail.FirstLineOfAddress;
            basesegment.FirstName = baseSegmentDetail.FirstName;
            basesegment.GenerationCode = baseSegmentDetail.GenerationCode;
            basesegment.HighestCredit = baseSegmentDetail.HighestCredit;
            basesegment.InterestTypeIndicator = baseSegmentDetail.InterestTypeIndicator;
            basesegment.LastReportDate = new TimeBucket(baseSegmentDetail.LastReportDate);
            basesegment.MiddleName = baseSegmentDetail.MiddleName;
            basesegment.OriginalChargeoffAmount = baseSegmentDetail.OriginalChargeoffAmount;
            basesegment.PaymentHistoryProfile = baseSegmentDetail.PaymentHistoryProfile;
            basesegment.PaymentRating = baseSegmentDetail.PaymentRating;
            basesegment.PortfolioType = baseSegmentDetail.PortfolioType;
            basesegment.ResidenceCode = baseSegmentDetail.ResidenceCode;
            basesegment.ScheduledMonthlyPaymentAmount = baseSegmentDetail.ScheduledMonthlyPaymentAmount;
            basesegment.SecondLineOfAddress = baseSegmentDetail.SecondLineOfAddress;
            basesegment.SpecialComment = baseSegmentDetail.SpecialComment;
            basesegment.SSN = baseSegmentDetail.SSN;
            basesegment.State = baseSegmentDetail.State;
            basesegment.SurName = baseSegmentDetail.SurName;
            basesegment.TelephoneNumber = baseSegmentDetail.TelephoneNumber;
            basesegment.TermsDuration = baseSegmentDetail.TermsDuration;
            basesegment.TermsFrequency = baseSegmentDetail.TermsFrequency;
            basesegment.ZipCode = baseSegmentDetail.ZipCode;

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.BaseSegments, loanInformation.BaseSegments));

            return basesegment;
        }

        //public async Task<IBaseSegment> GetBaseSagementByLoanNumber(string loanNumber, string Id)
        //{
        //    return await Query.FirstOrDefaultAsync(loanInformation => loanInformation.TenantId == TenantService.Current.Id && loanInformation.LoanNumber == loanNumber).Result.BaseSegments.Where(i => i.Id == Id).FirstOrDefault();
        //}

        public async Task<IJOne> UpdateJOneSegment(string loanNumber, IJOneRequest J1)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Report Information for {loanNumber} not found");

            IJOne Joneinfo = null;

            Joneinfo = loanInformation.J1s.Where(a => a.Id == J1.Id).FirstOrDefault();

            if (Joneinfo == null)
                throw new NotFoundException($"J1 for {Joneinfo.Id} not found");

            Joneinfo.J11 = J1.J11;
            Joneinfo.J12 = J1.J12;
            Joneinfo.J13 = J1.J13;
            Joneinfo.LastReportDate = new TimeBucket(J1.LastReportDate);

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.J1s, loanInformation.J1s));

            return Joneinfo;
        }

        public async Task<IJTwo> UpdateJTwoSegment(string loanNumber, IJTwoRequest J2)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Report Information for {loanNumber} not found");

            IJTwo jtwoinfo = null;

            jtwoinfo = loanInformation.J2s.Where(a => a.Id == J2.Id).FirstOrDefault();

            if (jtwoinfo == null)
                throw new NotFoundException($"J2 for {jtwoinfo.Id} not found");

            jtwoinfo.J21 = J2.J21;
            jtwoinfo.J22 = J2.J22;
            jtwoinfo.J23 = J2.J23;
            jtwoinfo.LastReportDate = new TimeBucket(J2.LastReportDate);

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.J2s, loanInformation.J2s));

            return jtwoinfo;
        }

        public async Task<IKOne> UpdateKOneSegment(string loanNumber, IKOneRequest K1)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Report Information for {loanNumber} not found");

            IKOne koneinfo = null;

            koneinfo = loanInformation.K1.Where(a => a.Id == K1.Id).FirstOrDefault();

            if (koneinfo == null)
                throw new NotFoundException($"K1 for {koneinfo.Id} not found");

            koneinfo.K1 = K1.K1;
            koneinfo.LastReportDate = new TimeBucket(K1.LastReportDate);

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.K1, loanInformation.K1));

            return koneinfo;
        }

        public async Task<IKTwo> UpdateKTwoSegment(string loanNumber, IKTwoRequest K2)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Report Information for {loanNumber} not found");

            IKTwo ktwoinfo = null;

            ktwoinfo = loanInformation.K2.Where(a => a.Id == K2.Id).FirstOrDefault();

            if (ktwoinfo == null)
                throw new NotFoundException($"K2 for {ktwoinfo.Id} not found");

            ktwoinfo.K2 = K2.K2;
            ktwoinfo.LastReportDate = new TimeBucket(K2.LastReportDate);

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.K2, loanInformation.K2));

            return ktwoinfo;
        }

        public async Task<IKThree> UpdateKThreeSegment(string loanNumber, IKThreeRequest K3)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Report Information for {loanNumber} not found");

            IKThree kthreeinfo = null;

            kthreeinfo = loanInformation.K3.Where(a => a.Id == K3.Id).FirstOrDefault();

            if (kthreeinfo == null)
                throw new NotFoundException($"K3 for {kthreeinfo.Id} not found");

            kthreeinfo.K3 = K3.K3;
            kthreeinfo.LastReportDate = new TimeBucket(K3.LastReportDate);

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.K3, loanInformation.K3));

            return kthreeinfo;
        }

        public async Task<IKFour> UpdateKFourSegment(string loanNumber, IKFourRequest K4)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Report Information for {loanNumber} not found");

            IKFour kfourinfo = null;

            kfourinfo = loanInformation.K4.Where(a => a.Id == K4.Id).FirstOrDefault();

            if (kfourinfo == null)
                throw new NotFoundException($"K4 for {kfourinfo.Id} not found");

            kfourinfo.K4 = K4.K4;
            kfourinfo.LastReportDate = new TimeBucket(K4.LastReportDate);

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.K4, loanInformation.K4));

            return kfourinfo;
        }

        public async Task<ILOne> UpdateLOneSegment(string loanNumber, ILOneRequest L1)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Report Information for {loanNumber} not found");

            ILOne loneinfo = null;

            loneinfo = loanInformation.L1.Where(a => a.Id == L1.Id).FirstOrDefault();

            if (loneinfo == null)
                throw new NotFoundException($"L1 for {loneinfo.Id} not found");

            loneinfo.L1 = L1.L1;

            loneinfo.LastReportDate = new TimeBucket(L1.LastReportDate);

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.L1, loanInformation.L1));

            return loneinfo;
        }

        public async Task<INOne> UpdateNOneSegment(string loanNumber, INOneRequest N1)
        {
            var loanInformation = await GetLoanInformationByLoanNumber(loanNumber);

            if (loanInformation == null)
                throw new NotFoundException($"Report Information for {loanNumber} not found");

            INOne noneinfo = null;

            noneinfo = loanInformation.N1.Where(a => a.Id == N1.Id).FirstOrDefault();

            if (noneinfo == null)
                throw new NotFoundException($"N1 for {noneinfo.Id} not found");

            noneinfo.N1 = N1.N1;

            noneinfo.LastReportDate = new TimeBucket(N1.LastReportDate);

            await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                   a.LoanNumber == loanNumber),
                Builders<IBureauMaster>.Update.Set(a => a.N1, loanInformation.N1));

            return noneinfo;
        }

        public async Task<IBaseSegment> UpdateMultipleColumn(List<IUpdateRequest> updateRequest, string column, string value)
        {
            IBaseSegment baseinfo = null;
            foreach (var item in updateRequest)
            {
                var loanInformation = await GetLoanInformationByLoanNumber(item.LoanNumber);
                if (loanInformation == null)
                    throw new NotFoundException($"Report Information for {item.LoanNumber} not found");

                baseinfo = loanInformation.BaseSegments.Where(a => a.Id == item.ReportId).FirstOrDefault();

                if (baseinfo == null)
                    throw new NotFoundException($"Basesegment for {item.ReportId} not found");

                switch (column.ToUpper())
                {
                    case "ACCOUNTTYPE":
                        baseinfo.AccountType = value;
                        break;
                    case "INTERESTTYPEINDICATOR":
                        baseinfo.InterestTypeIndicator = value;
                        break;
                    case "ACCOUNTSTATUS":
                        baseinfo.AccountStatus = value;
                        break;
                    case "PAYMENTRATING":
                        baseinfo.PaymentRating = value;
                        break;
                    case "COMPLIANCECONDITIONCODE":
                        baseinfo.ComplianceConditionCode = value;
                        break;
                    default:
                        break;
                }


                await Collection.UpdateOneAsync(Builders<IBureauMaster>.Filter.Where(a => a.TenantId == TenantService.Current.Id &&
                                                                       a.LoanNumber == item.LoanNumber),
                    Builders<IBureauMaster>.Update.Set(a => a.BaseSegments, loanInformation.BaseSegments));
            }


            return baseinfo;
        }

        public async Task<List<string>> GetLoanListByFileId(string fileId)
        {
            List<string> num = new List<string>();
            var details = await Query.ToListAsync();
            foreach (var item in details)
            {
                foreach (var item1 in item.BaseSegments)
                {
                    if (item1.FileId == fileId)
                        num.Add(item.LoanNumber);
                }
            }
            // return await Query.Where(loanInformation => loanInformation.TenantId == TenantService.Current.Id && loanInformation.BaseSegments.Any(x => x.FileId == FileId)).Select(x => x.LoanNumber).ToListAsync();
            return num;
        }

        public async Task<List<IBureauMaster>> GetLoanListforFile(string fileId)
        {
            List<IBureauMaster> num = new List<IBureauMaster>();
            var details = await Query.ToListAsync();
            foreach (var item in details)
            {
                foreach (var item1 in item.BaseSegments)
                {
                    if (item1.FileId == fileId)
                        num.Add(item);
                }
            }
            // return await Query.Where(loanInformation => loanInformation.TenantId == TenantService.Current.Id && loanInformation.BaseSegments.Any(x => x.FileId == FileId)).Select(x => x.LoanNumber).ToListAsync();
            return num;
        }

        //public async Task<List<IAccrual>> GetHistory(string loanNumber, string scheduleVersion)
        //{
        //    return await Query.Where(i => i.LoanNumber == loanNumber && i.ScheduleVersion == scheduleVersion).OrderByDescending(j => j.ProcessingDate).ToListAsync();
        //}

    }
}