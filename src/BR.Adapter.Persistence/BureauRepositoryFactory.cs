﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using BR.Adapter.Abstractions;
using LendFoundry.Security.Encryption;

namespace BR.Adapter.Persistence
{
    public class BureauRepositoryFactory : IBureauRepositoryFactory
    {
        public BureauRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IBureauRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            var encryptionService = Provider.GetService<IEncryptionService>();
            return new BureauRepository(tenantService, mongoConfiguration, encryptionService);
        }
    }
}
