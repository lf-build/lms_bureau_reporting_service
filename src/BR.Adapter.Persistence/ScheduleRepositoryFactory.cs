﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using BR.Adapter.Abstractions;
using LendFoundry.Security.Encryption;

namespace BR.Adapter.Persistence
{
    public class ScheduleRepositoryFactory : IScheduleRepositoryFactory
    {
        public ScheduleRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IScheduleRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            
            return new ScheduleDetailsRepository(tenantService, mongoConfiguration);
        }
    }
}
