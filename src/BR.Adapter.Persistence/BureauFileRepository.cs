﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver.Linq;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using System;
using MongoDB.Driver;
using BR.Adapter.Abstractions;
using LendFoundry.Security.Encryption;
using CreditExchange.StatusManagement;
using LMS.LoanAccounting;
using LMS.Loan.Filters.Abstractions;
using System.Collections.Generic;

namespace BR.Adapter.Persistence
{
    public class BureauFileRepository : MongoRepository<IBureauFile, BureauFile>, IBureauFileRepository
    {
        static BureauFileRepository()
        {
            BsonClassMap.RegisterClassMap<BureauFile>(map =>
            {
                map.AutoMap();
                var type = typeof(BureauFile);

                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

        }
        public BureauFileRepository(ITenantService tenantService, IMongoConfiguration configuration, IEncryptionService encrypterService)
            : base(tenantService, configuration, "BureauFile")
        {

        }
        public async Task<List<IBureauFile>> GetFileList()
        {
            return await Query.Where(fileInformation => fileInformation.TenantId == TenantService.Current.Id).ToListAsync();
        }
        public async Task<IBureauFile> GetFileByFileId(string Id)
        {
            return await Query.FirstOrDefaultAsync(loanInformation => loanInformation.TenantId == TenantService.Current.Id && loanInformation.FileId == Id);
        }


    }
}