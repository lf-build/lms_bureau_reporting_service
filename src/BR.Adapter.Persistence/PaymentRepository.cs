﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using BR.Adapter.Abstractions;

namespace BR.Adapter.Persistence
{
    public class PaymentRepository : MongoRepository<LMS.LoanAccounting.IPayment, LMS.LoanAccounting.Payment>, IPaymentRepository
    {
        static PaymentRepository()
        {
            BsonClassMap.RegisterClassMap<Payment_>(map =>
            {
                map.AutoMap();
                var type = typeof(LMS.LoanAccounting.Payment);
                map.MapMember(m => m.PaymentDate).SetSerializer(new DateTimeSerializer(true));
                map.MapMember(m => m.EffectiveDate).SetSerializer(new DateTimeSerializer(true));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public PaymentRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "payment-history")
        {
            CreateIndexIfNotExists("entity-id", Builders<LMS.LoanAccounting.IPayment>.IndexKeys.Ascending(i => i.TenantId));//.Ascending(i => i.EntityId).Ascending(i => i.LocNumber));
        }

        public async Task<List<LMS.LoanAccounting.IPayment>> GetPayments(string loanNumber)
        {
            return await Query.Where(i => i.LoanNumber == loanNumber).ToListAsync();
        }

        public async Task<DeleteResult> RemoveHistory()
        {
            return await Collection.DeleteManyAsync(Builders<LMS.LoanAccounting.IPayment>.Filter.Where(a => a.TenantId == TenantService.Current.Id));
        }

    }
}