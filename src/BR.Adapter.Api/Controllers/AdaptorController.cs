﻿using LendFoundry.Foundation.Services;
using BR.Adapter.Api.Controllers.CustomActions;
using Microsoft.AspNet.Mvc;
using System;
using System.Threading.Tasks;
using BR.Adapter.Abstractions;
using System.Collections.Generic;
using System.Net;
using System.Linq;

namespace BR.Adapter.Api.Controllers
{
    [Route("/")]
    public class AdapterController : ExtendedController
    {

        public AdapterController(IAdapterService adapterService, IBureauService bureauService)
        {
            AdapterService = adapterService;
            BureauService = bureauService;
        }

        private IAdapterService AdapterService { get; }
        private IBureauService BureauService { get; }

        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        [HttpGet("/exctract/loan")]
        public async Task<IActionResult> ExtractLoanInformation()
        {
            try
            {
                return Ok(await AdapterService.ExctractLoanInformation());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/process/loan")]
        public async Task<IActionResult> processloans()
        {
            try
            {
                return Ok(await BureauService.ProcessLoans());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/generatefile/{fileId}")]
        public async Task<IActionResult> Generatefile(string fileId)
        {
            try
            {
                return Ok(await BureauService.CreateFile(fileId));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/summary/{loannumber}")]
        public async Task<IActionResult> GetBureauSummaryByLoannumber(string loannumber)
        {
            try
            {
                return Ok(await BureauService.GetBureauSummaryByLoannumber(loannumber));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/all/files")]
        public async Task<IActionResult> GetAllFiles()
        {
            try
            {
                return Ok(await BureauService.GetAllFiles());
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/file/{fileid}")]
        public async Task<IActionResult> GetFileDetailsByFileId(string fileid)
        {
            try
            {
                return Ok(await BureauService.GetFileDetailsByFileId(fileid));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/all/{loannumber}/{fileid}")]
        public async Task<IActionResult> GetAllSegment(string loannumber, string fileid)
        {
            try
            {
                return Ok(await BureauService.GetAllSegment(loannumber, fileid));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/basesegment/{loannumber}/{fileid}")]
        public async Task<IActionResult> GetBaseSegment(string loannumber, string fileid)
        {
            try
            {
                return Ok(await BureauService.GetBaseSegment(loannumber, fileid));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpPut("/report/{loanNumber}/basesegment")]
        public async Task<IActionResult> UpdateBaseSegment(string loanNumber, [FromBody]BaseSegmentRequest basesegment)
        {
            try
            {
                return Ok(await BureauService.UpdateBaseSegment(loanNumber, basesegment));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/J1/{loannumber}/{fileid}")]
        public async Task<IActionResult> GetJ1Segment(string loannumber, string fileid)
        {
            try
            {
                return Ok(await BureauService.GetJOneSegment(loannumber, fileid));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/J2/{loannumber}/{fileid}")]
        public async Task<IActionResult> GetJ2Segment(string loannumber, string fileid)
        {
            try
            {
                return Ok(await BureauService.GetJTwoSegment(loannumber, fileid));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/K1/{loannumber}/{fileid}")]
        public async Task<IActionResult> GetKOneSegment(string loannumber, string fileid)
        {
            try
            {
                return Ok(await BureauService.GetKOneSegment(loannumber, fileid));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/K2/{loannumber}/{fileid}")]
        public async Task<IActionResult> GetKTwoSegment(string loannumber, string fileid)
        {
            try
            {
                return Ok(await BureauService.GetKTwoSegment(loannumber, fileid));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/K3/{loannumber}/{fileid}")]
        public async Task<IActionResult> GetKThreeSegment(string loannumber, string fileid)
        {
            try
            {
                return Ok(await BureauService.GetKThreeSegment(loannumber, fileid));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/K4/{loannumber}/{fileid}")]
        public async Task<IActionResult> GetKFourSegment(string loannumber, string fileid)
        {
            try
            {
                return Ok(await BureauService.GetKFourSegment(loannumber, fileid));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/L1/{loannumber}/{fileid}")]
        public async Task<IActionResult> GetLOneSegment(string loannumber, string fileid)
        {
            try
            {
                return Ok(await BureauService.GetLOneSegment(loannumber, fileid));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/report/N1/{loannumber}/{fileid}")]
        public async Task<IActionResult> GetNOneSegment(string loannumber, string fileid)
        {
            try
            {
                return Ok(await BureauService.GetNOneSegment(loannumber, fileid));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpPut("/report/{loanNumber}/j1")]
        public async Task<IActionResult> UpdateJOneSegment(string loanNumber, [FromBody]JOneRequest J1)
        {
            try
            {
                return Ok(await BureauService.UpdateJOneSegment(loanNumber, J1));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpPut("/report/{loanNumber}/j2")]
        public async Task<IActionResult> UpdateJTwoSegment(string loanNumber, [FromBody]JTwoRequest J2)
        {
            try
            {
                return Ok(await BureauService.UpdateJTwoSegment(loanNumber, J2));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpPut("/report/{loanNumber}/k1")]
        public async Task<IActionResult> UpdateKOneSegment(string loanNumber, [FromBody]KOneRequest K1)
        {
            try
            {
                return Ok(await BureauService.UpdateKOneSegment(loanNumber, K1));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpPut("/report/{loanNumber}/k2")]
        public async Task<IActionResult> UpdateKTwoSegment(string loanNumber, [FromBody]KTwoRequest K2)
        {
            try
            {
                return Ok(await BureauService.UpdateKTwoSegment(loanNumber, K2));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpPut("/report/{loanNumber}/k3")]
        public async Task<IActionResult> UpdateKThreeSegment(string loanNumber, [FromBody]KThreeRequest K3)
        {
            try
            {
                return Ok(await BureauService.UpdateKThreeSegment(loanNumber, K3));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpPut("/report/{loanNumber}/k4")]
        public async Task<IActionResult> UpdateKFourSegment(string loanNumber, [FromBody]KFourRequest K4)
        {
            try
            {
                return Ok(await BureauService.UpdateKFourSegment(loanNumber, K4));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpPut("/report/{loanNumber}/l1")]
        public async Task<IActionResult> UpdateLOneSegment(string loanNumber, [FromBody]LOneRequest L1)
        {
            try
            {
                return Ok(await BureauService.UpdateLOneSegment(loanNumber, L1));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpPut("/report/{loanNumber}/n1")]
        public async Task<IActionResult> UpdateNOneSegment(string loanNumber, [FromBody]NOneRequest N1)
        {
            try
            {
                return Ok(await BureauService.UpdateNOneSegment(loanNumber, N1));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpPut("/report/multipleupdate")]
        public async Task<IActionResult> UpdateMultipleLoans([FromBody]MultipleUpdateRequest updateRequest)
        {
            try
            {
                return Ok(await BureauService.UpdateMultipleLoans(updateRequest));
            }
            catch (Exception exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }



    }
}
