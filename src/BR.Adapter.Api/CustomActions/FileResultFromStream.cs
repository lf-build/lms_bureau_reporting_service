﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using BR.Adapter.Api.Utilities;

namespace BR.Adapter.Api.Controllers.CustomActions
{
    public class FileResultFromStream : ActionResult
    {
        public FileResultFromStream(string fileDownloadName, Stream fileStream, string contentType = null)
        {
            FileDownloadName = fileDownloadName;
            FileStream = fileStream;
            ContentType = contentType ?? MimeMapping.GetMimeMapping(Path.GetExtension(fileDownloadName));
        }

        private string ContentType { get; }
        private string FileDownloadName { get; }
        private Stream FileStream { get; }

        public override async Task ExecuteResultAsync(ActionContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = ContentType;
            response.Headers.Add("Content-Disposition", new[] { "attachment; filename=" + FileDownloadName });
            await FileStream.CopyToAsync(context.HttpContext.Response.Body);
            FileStream.Dispose();
        }
    }

}
