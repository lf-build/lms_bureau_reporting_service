﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using LendFoundry.Configuration.Client;
using BR.Adapter.Persistence;
using LendFoundry.NumberGenerator.Client;
using LMS.Foundation.Amortization;
using CreditExchange.StatusManagement.Client;
using LMS.LoanAccounting.Client;
using LendFoundry.ProductConfiguration.Client;
using BR.Adapter.Abstractions;
using LendFoundry.Security.Encryption;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.ProductRule.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Calendar.Client;
using LMS.LoanManagement.Client;
using LMS.Loan.Filters.Client;

namespace BR.Adapter.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddAmortization();
            services.AddEncryptionHandler();
            services.AddConfigurationService<AdapterConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            //services.AddConfigurationService<CalendarConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, "calendar");
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            //services.AddNumberGeneratorService(Settings.NumberGenerator.Host, Settings.NumberGenerator.Port);
            services.AddStatusManagementService(Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddAccrualBalance(Settings.LoanAccounting.Host, Settings.LoanAccounting.Port);
            services.AddProductService(Settings.Product.Host, Settings.Product.Port);
            //services.AddLookupService(Settings.Lookup.Host, Settings.Lookup.Port);
            services.AddDataAttributes(Settings.DataAttributes.Host, Settings.DataAttributes.Port);
            services.AddProductRuleService(Settings.ProductRule.Host, Settings.ProductRule.Port);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddCalendarService(Settings.Calendar.Host, Settings.Calendar.Port);
            services.AddLoanManagementService(Settings.LoanManagement.Host, Settings.LoanManagement.Port);
            services.AddLoanFilterService(Settings.LoanFilters.Host, Settings.LoanFilters.Port);
            // interface resolvers
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
            services.AddTransient(provider => provider.GetService<IConfigurationService<AdapterConfiguration>>().Get());
            //services.AddTransient<IConfiguration, Configuration>();
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));

            services.AddTransient<IAdapterService, AdapterService>();
            services.AddTransient<IAdapterServiceFactory, AdapterServiceFactory>();

            services.AddTransient<IAdapterRepository, AdapterRepository>();
            services.AddTransient<IAdapterRepositoryFactory, AdapterRepositoryFactory>();

            services.AddTransient<IBureauService, BureauService>();
            services.AddTransient<IBureauServiceFactory, BureauServiceFactory>();

            services.AddTransient<IBureauRepository, BureauRepository>();
            services.AddTransient<IBureauRepositoryFactory, BureauRepositoryFactory>();

            services.AddTransient<IPaymentRepository, PaymentRepository>();
            services.AddTransient<IPaymentRepositoryFactory, PaymentRepositoryFactory>();
            services.AddTransient<IScheduleRepository, ScheduleDetailsRepository>();
            services.AddTransient<IScheduleRepositoryFactory, ScheduleRepositoryFactory>();

            services.AddTransient<IAccrualDetailsRepository, AccrualDetailsRepository>();
            services.AddTransient<IAccrualDetailsRepositoryFactory, AccrualDetailsRepositoryFactory>();

            services.AddTransient<IBureauFileRepository, BureauFileRepository>();
            services.AddTransient<IBureauFileRepositoryFactory, BureauFileRepositoryFactory>();

            services.AddTransient<IFileStorageService, SftpService>();
            services.AddTransient<IFileStorageServiceFactory, FileStorageServiceFactory>();


            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseEventHub();

            app.UseMvc();
            app.UseHealthCheck();
        }

        //public class RequestLoggingMiddleware
        //{
        //    private RequestDelegate Next { get; }

        //    public RequestLoggingMiddleware(RequestDelegate next)
        //    {
        //        Next = next;
        //    }

        //    public async Task Invoke(HttpContext context)
        //    {
        //        //The MVC middleware reads the body stream before we do, which
        //        //means that the body stream's cursor would be positioned at the end.
        //        // We need to reset the body stream cursor position to zero in order
        //        // to read it. As the default body stream implementation does not
        //        // support seeking, we need to explicitly enable rewinding of the stream:
        //        try
        //        {
        //            await Next.Invoke(context);
        //        }
        //        catch (Exception exception)
        //        {

        //        }
        //    }
        //}
    }
}
