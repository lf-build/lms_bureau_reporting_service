FROM registry.lendfoundry.com/base:beta8

LABEL RevisionNumber "<<RevisionNumber>>"

ADD ./src/BR.Adapter.Abstractions /app/BR.Adapter.Abstractions
WORKDIR /app/BR.Adapter.Abstractions
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/BR.Adapter.Persistence /app/BR.Adapter.Persistence
WORKDIR /app/BR.Adapter.Persistence
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/BR.Adapter /app/BR.Adapter
WORKDIR /app/BR.Adapter
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/BR.Adapter.Client /app/BR.Adapter.Client
WORKDIR /app/BR.Adapter.Client
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/BR.Adapter.Api /app/BR.Adapter.Api
WORKDIR /app/BR.Adapter.Api
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel